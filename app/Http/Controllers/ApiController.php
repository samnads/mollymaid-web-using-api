<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class ApiController extends Controller
{
    public function customer_api_call(Request $request, $endpoint)
    {
        // to handle any ajax requests
        $data = [];
        /*************************************************************** */
        // customize data for real API
        foreach ($request->all() as $key => $value) {
            if ($key == "cleaning_materials") {
                $value = (boolean) $value; // convert int (1/0) to boolean
            }
            $data['params'][$key] = $value;
        }
        /*************************************************************** */
        if ($endpoint == 'sign-up') {
            $data['params']['name'] = $request->signup_name;
            unset($data['params']['signup_name']);
            $data['params']['mobilenumber'] = $request->signup_mobile;
            unset($data['params']['signup_mobile']);
            $data['params']['email'] = $request->signup_email;
            unset($data['params']['signup_email']);
            $data['params']['password'] = $request->signup_password;
            unset($data['params']['signup_password']);
        }
        /*************************************************************** */
        $response = customerApiCall($endpoint, $data, $request->method());
        /*************************************************************** */
        if (@$response['status'] == 'success') {
            if ($endpoint == 'check-otp') {
                Session::put('customer_id', $response['data']['id']);
                Session::put('customer_token', $response['data']['token']);
                Session::put('customer_name', $response['data']['name']);
                Session::put('customer_email', $response['data']['emailId']);
                Session::put('customer_mobile', $response['data']['phone']);
            }
        }
        /*************************************************************** */
        if (@$response['result']['status'] == 'success') {
            if ($endpoint == 'check_otp') {
                $session_data = [
                    'customer_id' => $response['result']['UserDetails']['id'],
                    'customer_name' => $response['result']['UserDetails']['UserName'],
                    'customer_token' => $response['result']['UserDetails']['token'],
                    'customer_avatar' => $response['result']['UserDetails']['image'],
                    'customer_mobile' => $response['result']['UserDetails']['mobile'],
                    'customer_email' => $response['result']['UserDetails']['email']
                ];
                session($session_data);
                if (isset($response['result']['UserDetails']['default_address_id'])) {
                    Session::put('customer_default_address_id', $response['result']['UserDetails']['token']);
                }
            } else if ($endpoint == 'update_customer_data') {
                Session::put('customer_token', $response['result']['UserDetails']['token']);
                Session::put('customer_name', $response['result']['UserDetails']['UserName']);
                Session::put('customer_email', $response['result']['UserDetails']['email']);
            } else if ($endpoint == 'name_entry') {
                Session::put('customer_token', $response['result']['UserDetails']['token']);
                Session::put('customer_name', $response['result']['UserDetails']['UserName']);
                Session::put('customer_email', $response['result']['UserDetails']['email']);
            } else if ($endpoint == 'update_avatar') {
                Session::put('customer_avatar', $response['result']['UserDetails']['image']);
            } else if ($endpoint == 'sign-up') {
                //Session::put('customer_id', $response['result']['UserDetails']['id']);
                //Session::put('customer_token', $response['result']['UserDetails']['token']);
                //Session::put('customer_name', $response['result']['UserDetails']['name']);
                //Session::put('customer_email', $response['result']['UserDetails']['email']);
            } else if ($endpoint == 'sign-in-with-mobile') {
                if ($response['result']['customer']['mobile_status'] = 1) {
                    Session::put('customer_id', $response['result']['customer']['id']);
                    Session::put('customer_token', @$response['result']['customer']['token']);
                    Session::put('customer_name', $response['result']['customer']['name']);
                    Session::put('customer_email', @$response['result']['customer']['email']);
                }
            }
        }
        /*************************************************************** */
        if ($endpoint == 'customer_logout') {
            Session::flush();
        }
        /*************************************************************** */
        return $response;
    }
}
