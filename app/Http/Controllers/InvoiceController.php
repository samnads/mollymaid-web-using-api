<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Config;
use Session;

class InvoiceController extends Controller
{
    public function invoice_payment_entry(Request $request)
    {
        $data = $request->all();
        $data['api_data'] = customerApiCall('data', [])['result'];
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $url = 'https://booking.emaid.info:3443/api/elitemaids/public/api/get-customer-odoo';
        //$url = Config::get('api.api_url') . 'get-customer-odoo';
        $response = $client->request('POST', $url, [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'customerId' => $data['id'],
            ],

        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $data['customer_odoo'] = $responseBody['data'];
        return view('invoice.invoice-payment-entry', $data);
    }
    public function invoice_payment_save(Request $request)
    {
        $data = $request->all();
        $data['api_data'] = customerApiCall('data', [])['result'];
        $client = new \GuzzleHttp\Client([
            'verify' => false
        ]);
        $url = 'https://booking.emaid.info:3443/api/elitemaids/public/api/save-invoice-pay';
        //$url = Config::get('api.api_url') . 'save-invoice-pay';
        $response = $client->request('POST', $url, [
            'headers' => [
                'cache-control' => 'no-cache',
                'Content-Type' => 'application/x-www-form-urlencoded'
            ],
            'form_params' => [
                'inv_id' => $request->input('inv_id'),
                'customerId' => $request->input('customerId'),
                'amount' => $request->input('amount'),
                'description' => $request->input('description')
            ]
        ]);
        $responseBody = json_decode((string) $response->getBody(), true);
        $data['save_invoice_pay'] = $responseBody['data'];
        //dd($data);
        return view('invoice.invoice-payment-checkout', $data);
    }
}
