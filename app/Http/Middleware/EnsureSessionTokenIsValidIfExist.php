<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureSessionTokenIsValidIfExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if (session('customer_id') || $request->user_id) {
            $params['params']['id'] = $request->user_id ?: session('customer_id');
            $params['params']['token'] = $request->token ?: session('customer_token');
            $response = customerApiCall('validate_token', $params);
            if (@$response['result']['status'] == "success") {
                $session_data = [
                    'customer_id' => $response['result']['customer']['customer_id'],
                    'customer_name' => $response['result']['customer']['customer_name'],
                    'customer_token' => $response['result']['customer']['oauth_token'],
                    'customer_avatar' => $response['result']['customer']['customer_photo_file'],
                    'customer_mobile' => $response['result']['customer']['mobile_number_1'],
                    'customer_email' => $response['result']['customer']['email_address'],
                ];
                session($session_data);
                return $next($request);
            } else {
                $request->session()->flush();
                return redirect()->route('home', ['error' => 'expired_token']);
            }
        } else {
            return $next($request);
        }
    }
}
