$().ready(function () {
    if (_id != null) {
        fetchAddressList();
    }
    $('input[name="payment_method"]').change(function () {
        let payment_method = this.value;
        let booking_btn = $('#booking-form button[type="submit"]');
        $("div[class*='payment-type-']").hide();
        //$('[class^=payment-type-]').hide(); // hide all for toggle specific on next step
        $('.payment-type-' + payment_method + '').show();
        $('[id^=submit-btn-pay-mode-]').hide();
        booking_btn.attr('disabled', false);
        if (payment_method == 1) {
            _checkout_token_data = undefined;
            $('#submit-btn-pay-mode-1').show();
            booking_btn.html('Complete');
        }
        else if (payment_method == 2) {
            _checkout_token_data = undefined;
            $('#submit-btn-pay-mode-1').show();
            booking_btn.html('Pay Now');
        }
        else if (payment_method == 3) {
            _checkout_token_data = undefined;
            booking_btn.html('');
            $('#submit-btn-pay-mode-3').show();
        }
        else if (payment_method == 4) {
            _checkout_token_data = undefined;
            booking_btn.html('');
            $('#submit-btn-pay-mode-4').show();
        }
        else if (payment_method == 5) {
            _checkout_token_data = undefined;
            $('#submit-btn-pay-mode-' + payment_method, booking_form).show();
            TamaraTokenization();
        }
        calculate();
    });
    $('textarea[name="instructions"]', booking_form).change(function () {
        if (this.value != '') {
            $('#booking-summary .instructions p.instructions').text(this.value);
            $('#booking-summary .instructions').show();
        }
        else {
            $('#booking-summary .instructions').hide();
        }
    });
    coupon_apply_form = $('#coupon-apply-form');
    coupon_form_validator = $('#coupon-apply-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "coupon_code": {
                required: true,
            },
        },
        messages: {
            "coupon_code": {
                required: "Enter coupon code",
            },
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function (form) {
            let submit_btn = $('button[type="submit"]', form);
            submit_btn.html(loading_button_html).prop("disabled", true);
            _cart.coupon_code = $('input[name="coupon_code"]', form).val();
            calculate();
        }
    });
    // apple pay avilability check
    debug = true;
    function logit(data) {
        if (debug == true) {
            console.log(data);
        }
    };
    var merchantIdentifier = "merchant.com.emaid.elitmaidsUser.livennew";
    if (window.ApplePaySession) {
        var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
        promise.then(function (canMakePayments) {
            if (canMakePayments) {
                // Display Apple Pay button here.
                logit('hi, I can do ApplePay');
            }
        });
    } else {
        logit('ApplePay is not available on this browser');
        $('.pay-mode-li-3').hide(); // hide apple pay
    }
    /*********************************************************************************************/
});
function checkoutTokenization() {
    booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.html(loading_button_html);
    /********************************* Tokenization ************************************/
    $.ajax({
        type: 'POST',
        url: _checkout_token_url,
        dataType: 'json',
        data: JSON.stringify({
            "type": "card",
            "requestSource": "JS",
            "number": $('#card-details input[name="card_number"]').val(),
            "expiry_month": moment($('#card-details input[name="exp_date"]').val(), "MM/YY").format("MM"),
            "expiry_year": moment($('#card-details input[name="exp_date"]').val(), "MM/YY").format("YYYY"),
            "cvv": $('#card-details input[name="cvv"]').val()
        }),
        // important
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + _checkout_primary_key,
        },
        crossDomain: true,
        contentType: 'application/json;charset=UTF-8',
        success: function (response) {
            _checkout_token_data = response;
            // submit booking form again
            if (typeof createBooking === "function") {
                createBooking();
            }
            else {
                $('#booking-form').submit();
            }
        },
        error: function (response) {
            toast('Failed', 'Invalid card details', 'error');
            _checkout_token_data = undefined;
            booking_btn.html('Pay Now');
        },
    });
}
function TamaraTokenization() {
    booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.html(loading_button_html).attr('disabled', true);
    /********************************* Tokenization ************************************/
    $.ajax({
        type: 'GET',
        url: "https://api-sandbox.tamara.co/checkout/payment-types",
        dataType: 'json',
        data: {
            "country": "AE",
            "currency": "AED",
            "order_value": _calculation_data.summary.total_payable
        },
        // important
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhY2NvdW50SWQiOiJkNTJjMjMzZC05MmFjLTRlMTYtYTc1My0yNTc3ZWZjY2Q1YmYiLCJ0eXBlIjoibWVyY2hhbnQiLCJzYWx0IjoiZjQzOWE1YTVjNzY3ZTgzZTdmOGEwNzgyYTBkYzVkMTgiLCJpYXQiOjE2NTU5ODk3ODAsImlzcyI6IlRhbWFyYSJ9.LIIEkaOTy_quWbdNrb8g0F2gBI3O3GC29jjI-FDK904IFGDslIT7-JlGYqhffd6YY3FeNCYFEuKwSQS-yFwwmufH-Rjs95GxRNPWPkwrwXCzxuqyzxfxTMbcUxJ6O3FFKgZ3Hzl3NG9Rt0rDkr9vJIHrx8zMscJiCnMR-dRDLJYvyJXoJrrM3j1XsqiT1Y0JFm2eNXVjnPgvN1PsMHCaSBX3n3BNfgcFM3gUQNA1wAQg5YUKLOWFpSNLKpPXaEOy7cmmVGhi1yVzhAcGa_IK-Wfh9imFsqYqb6QQ-oQ2ZS3WhiNjeO0KX_LusYcMIyRh0luf5HLDCENFNq99a3K3NA',
        },
        crossDomain: true,
        contentType: 'application/json;charset=UTF-8',
        success: function (response) {
            console.log(response.length);
            if (response.length == 0) {
                showTextAlert('Not Eligible for Tamara Payment');
                booking_btn.attr('disabled', true);
                return false;
            }
            else {
                booking_btn.attr('disabled', false);
                booking_btn.html('Pay Now');
                $.ajax({
                    type: 'POST',
                    url: "https://api-sandbox.tamara.co/checkout/payment-options-pre-check",
                    dataType: 'json',
                    data: JSON.stringify({
                        "country": "AE",
                        "order_value": { amount: _calculation_data.summary.total_payable, currency: "AED" }
                    }),
                    // important
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhY2NvdW50SWQiOiJkNTJjMjMzZC05MmFjLTRlMTYtYTc1My0yNTc3ZWZjY2Q1YmYiLCJ0eXBlIjoibWVyY2hhbnQiLCJzYWx0IjoiZjQzOWE1YTVjNzY3ZTgzZTdmOGEwNzgyYTBkYzVkMTgiLCJpYXQiOjE2NTU5ODk3ODAsImlzcyI6IlRhbWFyYSJ9.LIIEkaOTy_quWbdNrb8g0F2gBI3O3GC29jjI-FDK904IFGDslIT7-JlGYqhffd6YY3FeNCYFEuKwSQS-yFwwmufH-Rjs95GxRNPWPkwrwXCzxuqyzxfxTMbcUxJ6O3FFKgZ3Hzl3NG9Rt0rDkr9vJIHrx8zMscJiCnMR-dRDLJYvyJXoJrrM3j1XsqiT1Y0JFm2eNXVjnPgvN1PsMHCaSBX3n3BNfgcFM3gUQNA1wAQg5YUKLOWFpSNLKpPXaEOy7cmmVGhi1yVzhAcGa_IK-Wfh9imFsqYqb6QQ-oQ2ZS3WhiNjeO0KX_LusYcMIyRh0luf5HLDCENFNq99a3K3NA',
                    },
                    crossDomain: true,
                    contentType: 'application/json;charset=UTF-8',
                    success: function (response) {
                        console.log(response.length);
                    },
                    error: function (response) {
                        console.log(response);
                    },
                });
            }
        },
        error: function (response) {
            console.log(response);
        },
    });
}
function goToStep(step) {
    $('div[class*=step-]').hide();
    $('.step-' + step).show();
}
function create_booking_error(response) {
    /**************************************************** */
    let title = "Error";
    let type = "error";
    let message = response.result.message;
    if (response.result.title) {
        title = response.result.title;
    }
    if (response.result.type) {
        type = response.result.type;
    }
    /**************************************************** */
    toast(title, message, type);
}