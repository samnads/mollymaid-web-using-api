// step button actions
let step_titles = [
    'Book Your Service',
    'Select Date & Time',
    'Login / Signup',
    'Add Address',
    'Checkout'
];
$('[data-action="next-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    if (isStepValid(current_step) === false) {
        // DONT'T GO TO NEXT STEP
        return;
    }
    let next_step = current_step + 1;
    //
    if (next_step == 2) {
        //available_timeRender();
    }
    else if (next_step == 3) {
        // login or signup step
        if (_id == null) {
            // Case 1 : not logged in
            //showLogin();
            //return false;
        }
        else {
            // logged in
            if (_default_address_id == null){
                // no default address
                next_step = 4;
            }
            else{
                // already have address
                next_step = 5;
            }
        }
    }
    else if (next_step == 4) {
        stepSignup();
        return false;
    }
    if (next_step == 4) {
        if (_default_address_id != null) {
            next_step = 5;
        }
    }
    //
    $('#step-title').html(step_titles[next_step - 1]); // step title
    //$('[data-action="prev-step-go"]').show(500);
    $('.step-' + current_step).hide(500);
    $('.step-' + next_step).show(500);
    _step = next_step;
    markStep(next_step);
    // do after going to step
    /************************************* */
    // SEO
    let _meta_title_new = _meta_title + ' ' + next_step + '/5';
    $(document).prop('title', _meta_title_new);
    /************************************* */
    if (next_step == 4) {
        $('input[name="payment_method"]:checked', booking_form).trigger('change');
    }
    //
    if (next_step == 3) {
        $('#frequency-popup').show(500);
    }
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', next_step)));
    window.scrollTo(0, 0);
});
/***************************************************************** */
// sign up from step
$('[data-action="step-signup"]').click(function () {
    $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/sign-up",
        dataType: 'json',
        //data: JSON.stringify($('#login-popup-form').serializeObjectForApi()),
        data: $('.register-section :input').serialize(),
        //contentType: 'application/json;charset=UTF-8',
        success: function (response) {
            if (response.result.status == "success") {
                console.log(response.data.mobile_verified);
                _id = response.result.UserDetails.id;
                _name = response.result.UserDetails.name;
                _email = response.result.UserDetails.emailId;
                
                //toast('Hi, <b>' + response.result.UserDetails.name + '</b>', 'Please add address to continue.', 'success');
                // add in url
                //let new_url = _current_url + $.query.set('login', "success").set('user_id', _id).set("token", response.result.UserDetails.token).toString();
                //window.history.pushState({}, "", decodeURI(new_url));
                /***************************************************** */
                if (response.data.mobile_verified == 0){
                    $('#login-otp-popup-form input[name="mobilenumber"]').val(response.result.UserDetails.mobile);
                    $('#login-otp-popup-form input[name="id"]').val(response.result.UserDetails.id);
                    toast('OTP Sent', 'Please enter the OTP to continue.', 'info');
                    $('#otp-popup').show(500);
                }
                else{
                    $('.step-' + _step).hide(500);
                    $('.step-' + (_step + 1)).show(500);
                    _step = _step + 1;
                    $('.before-login').hide();
                    $('.after-login').show();
                }
            }
            else {
                //submit_btn.html('Continue').prop("disabled", false);
                toast('Error', response.result.message, 'error');
            }
        },
        error: function (response) {
            //submit_btn.html('Continue').prop("disabled", false);
        },
    });
});
// login from step
$('[data-action="step-login"]').click(function () {
    $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/sign-in-with-mobile",
        dataType: 'json',
        //data: JSON.stringify($('#login-popup-form').serializeObjectForApi()),
        data: $('.login-section :input').serialize(),
        //contentType: 'application/json;charset=UTF-8',
        success: function (response) {
            if (response.result.status == "success") {
                console.log(response);
                _id = response.result.UserDetails.id;
                _name = response.result.UserDetails.name;
                _email = response.result.UserDetails.emailId;
                $('.step-' + _step).hide(500);
                $('.step-' + (_step + 1)).show(500);
                _step = _step + 1;
                $('.before-login').hide();
                $('.after-login').show();
                toast('Hi, <b>' + response.result.UserDetails.name + '</b>', 'Please add address to continue.', 'success');
                // add in url
                let new_url = _current_url + $.query.set('login', "success").set('user_id', _id).set("token", response.result.UserDetails.token).toString();
                window.history.pushState({}, "", decodeURI(new_url));
            }
            else {
                //submit_btn.html('Continue').prop("disabled", false);
                toast('Error', response.result.message, 'error');
            }
        },
        error: function (response) {
            //submit_btn.html('Continue').prop("disabled", false);
        },
    });
});
// save address from step
$('[data-action="step-save-address"]').click(function () {
    $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/add_address",
        dataType: 'json',
        //data: JSON.stringify($('#login-popup-form').serializeObjectForApi()),
        data: $('.address-section :input').serialize(),
        //contentType: 'application/json;charset=UTF-8',
        success: function (response) {
            if (response.result.status == "success") {
                console.log(response);
                _default_address_id = response.result.address_list[0].address_id;
                $('[name="address_id"]', booking_form).val(_default_address_id); // set address id
                $('.step-' + _step).hide(500);
                $('.step-' + (_step + 1)).show(500);
                _step = _step + 1;
                toast('Saved', response.result.message, 'info');
            }
            else {
                //submit_btn.html('Continue').prop("disabled", false);
                toast('Error', response.result.message, 'error');
            }
        },
        error: function (response) {
            //submit_btn.html('Continue').prop("disabled", false);
        },
    });
});
/***************************************************************** */
$('[data-action="prev-step"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = Number($(this).attr('data-step'));
    let prev_step = current_step - 1;
    if (current_step == 5) {
        prev_step = 2;
    }
    if (current_step == 4) {
        // already logged in, that's why user on step 4
        prev_step = 2;
    }
    //
    $('#step-title').html(step_titles[prev_step - 1]); // step title
    $('.step-' + current_step).hide(500);
    $('.step-' + prev_step).show(500);
    _step = prev_step;
    markStep(_step);
    /************************************* */
    $(document).prop('title', _meta_title + ' ' + _step + '/5');
    /************************************* */
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', _step)));
    window.scrollTo(0, 0);
});
$('[data-action="prev-step-go"]').click(function () {
    booking_form = $('#booking-form');
    let current_step = _step;
    let prev_step = current_step - 1;
    //
    $('.step-' + current_step).hide(500);
    $('.step-' + prev_step).show(500);
    _step = prev_step;
    markStep(_step);
    if (_step == 1) {
        //$('[data-action="prev-step-go"]').hide(500);
    }
    /************************************* */
    $(document).prop('title', _meta_title + ' ' + prev_step + '/5');
    /************************************* */
    if (prev_step == 3) {
        //$('#frequency-popup').show(500);
    }
    window.history.pushState({}, "", decodeURI(_current_url + $.query.set('step', prev_step)));
    window.scrollTo(0, 0);
});
/************************************************************************************************* */
/************************************************************************************************* */
$(booking_form).submit(function () {
    // just to show last step error toasts
    isStepValid(4);
});
/************************************************************************************************* */
// steps validator
function isStepValid(step) {
    var required = [];
    if (step == 1) {
        required = [
            {
                name: 'hours',
                status: $('input[name="hours"]', booking_form).valid()
            },
            {
                name: 'professionals_count',
                status: $('input[name="professionals_count"]', booking_form).valid()
            },
            {
                name: 'cleaning_materials',
                status: $('input[name="cleaning_materials"]', booking_form).valid()
            },
        ];
    }
    else if (step == 2) {
        required = [
            {
                name: 'frequency',
                status: $('input[name="frequency"]', booking_form).valid()
            },
            {
                name: 'date',
                status: $('input[name="date"]', booking_form).valid()
            },
            {
                name: 'time',
                status: $('input[name="time"]', booking_form).valid()
            },
        ];
    }
    else if (step == 3) {
        required = [
            {
                name: 'date',
                status: $('input[name="date"]', booking_form).valid()
            },
            {
                name: 'time',
                status: $('input[name="time"]', booking_form).valid()
            },
        ];
    }
    else if (step == 4) {
        required = [
            {
                name: 'payment_method',
                status: $('input[name="payment_method"]', booking_form).valid()
            },
        ];
    }
    /************************************************ */
    // check for errors and show as toast
    var errors = required.filter(field => {
        return field.status === false
    });
    if (errors.length > 0) {
        var first_error_field = errors[0].name;
        var error_message = booking_form_validator.submitted[first_error_field];
        toast(null, error_message, 'info');
        scrollToElement($('[name="' + first_error_field + '"]', booking_form));
        return false;
    }
    /************************************************ */
    // step valid
    return true;
}
$('[data-action="save-step-address"]').click(function (e) {
    let current_step = Number($(this).attr('data-step'));
    if ($('.address-step [name="area_id"]').val() == "") {
        toast('Required', "Please select address area", 'warning');
        return false;
    }
    else if ($('.address-step [name="building"]').val() == "") {
        toast('Required', "Please enter building name", 'warning');
        return false;
    }
    else if ($('.address-step [name="unit_no"]').val() == "") {
        toast('Required', "Please enter unit number", 'warning');
        return false;
    }
    else if ($('.address-step [name="customer_address"]').val() == "") {
        toast('Required', "Please enter full address", 'warning');
        return false;
    }
    $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/add_address",
        dataType: 'json',
        data: $('.address-step :input').serialize(),// serialize only required
        success: function (response) {
            _address_list = response.result.address_list;
            //submit_btn.html(submit_btn_text).prop("disabled", false);
            if (response.result.status == "success") {
                //hideNewAddress();
                //fetchAddressList();
                toast('Saved', response.result.message, 'success');
                $('div[class*=" step-"]').hide();
                $('.step-' + (current_step + 1)).show();
                markStep(current_step + 1);
            }
            else {
                toast('Error', response.result.message, 'error');
            }
        },
        error: function (response) {
            ///submit_btn.html(submit_btn_text).prop("disabled", false);
        },
    });
});
/************************************************************************************************* */
function renderTimes(available_times) {
    var times_html = ``;
    // Check if available_times is empty
    if (available_times.length === 0) {
        times_html = `<div class="alert alert-info" role="alert">
                            <i class="fa fa-exclamation-circle" aria-hidden="true"></i> No time slots available for the selected date!
                    </div>`;
    } else {
        // Generate HTML for available times
        $.each(available_times, function (index, time) {
            times_html += `<li>
                        <input id="time-`+ index + `" value="` + time + `" name="time" class="" type="radio">
                        <label for="time-`+ index + `">
                            <!--<p>AED 5 Extra</p>-->`+ moment(time, "HH:mmm").format("hh:mm A") + `
                        </label>
                    </li>`;
        });
        times_html += `<div class="clear"></div>`;
    }
    $('#times-holder').html(times_html);
    $('#booking-form input[name="time"]').change(function () {
        var hours = $('#booking-form input[name="hours"]:checked').val();
        var summary_time_from = moment(this.value, 'H:mm').format('hh:mm A');
        var summary_time_to = moment(this.value, 'H:mm').add(hours, 'hours').format('hh:mm A');
        $('#booking-summary .time').html(summary_time_from + ` to ` + summary_time_to);
        calculate();
    });
    //$('#booking-form input[name="time"]:first').trigger('click');
}
available_time_req = null;
function available_timeRender() {
    $('#times-holder').html(loading_html);
    booking_form = $('#booking-form');
    available_time_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_time",
        data: {
            date: $('input[name="date"]', booking_form).val(),
            hours: $('input[name="hours"]:checked', booking_form).val()
        },
        dataType: 'json',
        beforeSend: function () {
            if (available_time_req != null) {
                available_time_req.abort();
            }
        },
        success: function (response) {
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
available_datetime_req = null;
function available_datetimeRender() {
    $('#times-holder').html(loading_html);
    booking_form = $('#booking-form');
    available_datetime_req = $.ajax({
        type: 'GET',
        url: _base_url + "api/customer/available_datetime",
        dataType: 'json',
        beforeSend: function () {
            if (available_datetime_req != null) {
                available_datetime_req.abort();
            }
        },
        success: function (response) {
            var dates_html = ``;
            $.each(response.result.available_dates, function (index, date) {
                let disabled = response.result.disabled_dates.find(disabled_date => {
                    return disabled_date == date
                });
                disabled = disabled ? 'disabled' : '';
                title = disabled ? 'Holiday' : '';
                dates_html += `<div class="item">
                    <input id="date-`+ index + `" value="` + moment(date, 'YYYY-MM-DD').format('DD/MM/YYYY') + `" name="date" class="" type="radio" ` + disabled + `>
                    <label for="date-`+ index + `" title="` + title + `">
                        <div class="calendar-tmb-main">
                            <div class="calendar-tmb-day">`+ moment(date, 'YYYY-MM-DD').format('ddd') + `</div>
                            <div class="calendar-tmb-date">`+ moment(date, 'YYYY-MM-DD').format('DD') + `</div>
                            <div class="calendar-tmb-month">`+ moment(date, 'YYYY-MM-DD').format('MMM') + `</div>
                        </div>
                    </label>
                </div>`;
            });
            $('#calendar').html(dates_html);
            $('#booking-form input[name="date"]').change(function () {
                var summary_date = moment(this.value, 'DD/MM/YYYY').format('DD MMM YYYY');
                $('#booking-summary .date').html(summary_date);
                available_timeRender();
                //calculate();
            });
            $('#calendar.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
            $('#calendar.owl-carousel').find('.owl-stage-outer').children().unwrap();
            $('#calendar.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
            $("#calendar.owl-carousel").owlCarousel({
                nav: true,
                loop: false,
                dots: false,
                margin: 15,
                autoplay: false,
                autoplayTimeout: 2000,
                smartSpeed: 800,
                autoplayHoverPause: false,
                responsive: {
                    0: {
                        items: 5
                    },
                    600: {
                        items: 5
                    },
                    1000: {
                        items: 10
                    },
                    1100: {
                        items: 10
                    },
                    1200: {
                        items: 10,
                        //margin: 50
                    }
                }
            }); //re-initialise the owl
            $('#booking-form input[name="date"]:first').trigger('click');
            renderTimes(response.result.available_times);
        },
        error: function (response) {
        },
    });
}
// frequency popup
$('#frequency-popup .popup-close').click(function () {
    $('#frequency-popup').hide(500);
});
$('input[name="frequency"]').change(function () {
    // frequency selected
    /*let frequency = $('input[name="frequency"]:checked').val();
    frequency_sel = _data.frequency_list.find(freq => {
        return freq.code == frequency
    });
    $('.frequency').html(frequency_sel.name);*/
    /*if (frequency_sel.coupon_code == null) {
        // frequency have no coupon code
        if (Cookies.get('coupon_code')) {
            // take from cookie if found
            _cart.coupon_code = Cookies.get('coupon_code');
        }
    }
    else {
        // frequency copon found
        Cookies.remove('coupon_code'); // remove from cookie
        _cart.coupon_code = frequency_sel.coupon_code;
    }*/
    let frequency = $(this).attr('data-name');
    $('.frequency_name').html(frequency);
    calculate();
});
$('[data-action="frequency-select"]').click(function () {
    // frequency selected
    let frequency = $('input[name="frequency"]:checked').val();
    frequency_sel = _data.frequency_list.find(freq => {
        return freq.code == frequency
    });
    $('.frequency').html(frequency_sel.name);
    if (frequency_sel.coupon_code == null) {
        // frequency have no coupon code
        if (Cookies.get('coupon_code')) {
            // take from cookie if found
            _cart.coupon_code = Cookies.get('coupon_code');
        }
    }
    else {
        // frequency copon found
        Cookies.remove('coupon_code'); // remove from cookie
        _cart.coupon_code = frequency_sel.coupon_code;
    }
    $('#frequency-popup').hide(500);
    calculate();
});
$('[data-action="frequency-popup"]').click(function () {
    $('#frequency-popup').show(500);
});
/************************************************************************************************* */
$('input[name="hours"]').click(function () {
    $('#booking-summary .duration').html(this.value + ' Hours');
    available_timeRender();
    calculate();
});
$('input[name="professionals_count"]').change(function () {
    $('#booking-summary .professionals_count').html(this.value);
    calculate();
});
$('input[name="cleaning_materials"]').change(function () {
    $('#booking-summary .is_materials_included').html(this.value == 1 ? 'Yes' : 'No');
    calculate();
});
$().ready(function () {
    $('.toggle-calendar1').pignoseCalendar({
        minDate: moment(),
        initialize: true,
        date: moment('2024-01-12'),
        select: function (date, context) {
            /**
             * @params this Element
             * @params date moment[]
             * @params context PignoseCalendarContext
             * @returns void
             */

            // This is selected button Element.
            var $this = $(this);

            // You can get target element in `context` variable, This element is same `$(this)`.
            var $element = context.element;

            // You can also get calendar element, It is calendar view DOM.
            var $calendar = context.calendar;

            // Selected dates (start date, end date) is passed at first parameter, And this parameters are moment type.
            // If you unselected date, It will be `null`.
            let pignoseCalendar_date = date[0];
            $('input[name="date"]', booking_form).val(pignoseCalendar_date.format('DD/MM/YYYY'));
            var summary_date = moment(pignoseCalendar_date).format('DD MMM YYYY');
            $('#booking-summary .date').html(summary_date);
            $('#booking-summary .step-2-show').show();
            available_timeRender();
        }
    });
    $('#booking-form input[name="hours"]:first').trigger('click');
    $('#booking-form input[name="professionals_count"]:first').trigger('click');
    $('#booking-form input[name="frequency"]:first').trigger('click');
    $('#booking-form input[name="cleaning_materials"][value="0"]').trigger('click');
    /*if (_coupon_code != null) {
        _cart.coupon_code = _coupon_code;
    }
    if (Cookies.get('coupon_code')) {
        _cart.coupon_code = Cookies.get('coupon_code');
    }
    $(".scroll").jScroll();
    /************************************************************************************
    $('#booking-form input[name="hours"]:first').trigger('click');
    $('#booking-form input[name="professionals_count"]:first').trigger('click');
    //$('#booking-form input[name="cleaning_materials"]:first').trigger('click');
    /************************************************************************************ 
    //available_datetimeRender();
    let frequency = $('input[name="frequency"]:checked').val();
    frequency_sel = _data.frequency_list.find(freq => {
        return freq.code == frequency
    });
    $('.frequency').html(frequency_sel.name);
    /************************** addons render  
    var addons_html = ``;
    if (_service_type_data.addons.length == 0) {
        $('.add-ons-wrapper').hide();
        $('#title-step-2').html('Instructions');
    }
    else {
        $.each(_service_type_data.addons, function (index, addon) {
            addons_html += `<div class="item">
                    <input id="addon_`+ addon.service_addons_id + `" value="` + addon.service_addons_id + `" name="addons[]" class=""
                        type="checkbox">
                    <label for="addon_`+ addon.service_addons_id + `">
                        <div class="add-ons-scroll-thumb">
                            <div class="add-ons-scroll-image"><img src="`+ addon.image_url + `"
                                    alt="" /></div>
                            <div class="add-ons-scroll-cont-main">
                                <div class="add-ons-scroll-cont">
                                    <h4>`+ addon.service_addon_name + `</h4>
                                    <p><strong><!--Messy cupboards?--></strong>
                                        `+ addon.service_addon_description + `<!--<a href="#">Learn
                                            More</a>--></p>
                                </div>
                                <div class="add-ons-scroll-price">AED <span> `+ addon.strike_amount + ` </span> ` + addon.amount + `</div>
                                <div class="add-ons-scroll-btn">
                                    <div class="addon-btn-main">
                                        <a class="sp-btn">Add</a>
                                    </div>
                                    <div class="addon-btn-count">
                                        <input data-addons_id="` + addon.service_addons_id + `" data-action="addon-minus" class="addon-btn-minus" type="button">
                                        <input
                                        id="addons_quanity_` + addon.service_addons_id + `"
                                        data-addons_id="` + addon.service_addons_id + `"
                                        min="0" max="` + addon.cart_limit + `"
                                        name="addons_quanity[]"
                                        value="0"
                                        class="addon-text-field no-arrow"
                                        type="number" readonly>
                                        <input data-addons_id="` + addon.service_addons_id + `" data-action="addon-plus" class="addon-btn-plus" type="button">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </label>
                </div>`;
        });
        $('#service-addons-holder').html(addons_html);
        $('#service-addons-holder.owl-carousel').trigger('destroy.owl.carousel'); //these 3 lines kill the owl, and returns the markup to the initial state
        $('#service-addons-holder.owl-carousel').find('.owl-stage-outer').children().unwrap();
        $('#service-addons-holder.owl-carousel').removeClass("owl-center owl-loaded owl-text-select-on");
        $("#service-addons-holder.owl-carousel").owlCarousel({
            nav: false,
            loop: false,
            dots: true,
            margin: 15,
            autoplay: false,
            autoplayTimeout: 2000,
            smartSpeed: 800,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                1100: {
                    items: 3
                },
                1200: {
                    items: 3,
                    //margin: 50
                }
            }
        }); //re-initialise the owl
    }
    /************************************************************************************************* */
    $('#booking-form input[name="addons[]"]').change(function () {
        // direct ui click change handler
        if (this.checked) {
            $('input[type="number"][data-addons_id="' + this.value + '"]').val(1);
        }
        else {
            $('input[type="number"][data-addons_id="' + this.value + '"]').val(0);
        }
        addonsToggled();
    });
    /************************************************************************************************* */
    function addonsToggled() {
        _cart.addons = [];
        $('#booking-form input[name="addons[]"]:checked').each(function (index, obj) {
            var addon = {
                service_addons_id: $(this).val(),
                quantity: $('#addons_quanity_' + $(this).val()).val(),
            }
            _cart.addons.push(addon);
        });
        calculate();
    }
    /************************************************************************************************* */
    $('[data-action="addon-plus"]').click(function () {
        current_quantity = Number($('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val());
        max_quantity = $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').attr("max");
        if (current_quantity >= max_quantity) {
            $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(max_quantity)
        }
        else {
            $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(current_quantity + 1)
        }
        addonsToggled();
    });
    /************************************************************************************************* */
    $('[data-action="addon-minus"]').click(function () {
        current_quantity = Number($('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val());
        min_quantity = $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').attr("min");
        updated_quantity = current_quantity - 1;
        if (updated_quantity == 0) {
            $('#addon_' + $(this).attr("data-addons_id")).prop('checked', false).trigger('change');
        }
        else if (updated_quantity > min_quantity) {
            $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(updated_quantity);
        }
        else {
            $('input[name="addons_quanity[]"][data-addons_id="' + $(this).attr("data-addons_id") + '"]').val(min_quantity);
        }
        addonsToggled();
    });
    /************************************************************************************************* */
    create_booking_req = null;
    booking_form_validator = $('#booking-form').validate({
        focusInvalid: false,
        ignore: [],
        rules: {
            "date": {
                required: true,
            },
            "time": {
                required: true,
            },
            "hours": {
                required: true,
            },
            "professionals_count": {
                required: true,
            },
            "cleaning_materials": {
                required: true,
            },
            "instructions": {
                required: false,
            },
            "payment_method": {
                required: true,
            },
            "card_number": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
            "exp_date": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
            "cvv": {
                required: 'input[name="payment_method"][value="2"]:checked'
            },
        },
        messages: {
            "date": {
                required: "Select service date",
            },
            "time": {
                required: "Select service time",
            },
            "hours": {
                required: "Select service hours",
            },
            "professionals_count": {
                required: "Select no. of professionals",
            },
            "cleaning_materials": {
                required: "Select materials option",
            },
            "instructions": {
                required: "Type any instructions",
            },
            "payment_method": {
                required: "Select payment method",
            },
            "card_number": {
                required: "Enter card number",
            },
            "exp_date": {
                required: "Enter card expiry date",
            },
            "cvv": {
                required: "Enter CVV code",
            },
        },
        errorPlacement: function (error, element) {
            if (element.attr("name") == "date") {
                error.insertAfter($('#calendar').parent());
            }
            else if (element.attr("name") == "time") {
                error.insertAfter($('#times-holder').parent());
            }
            else if (element.attr("name") == "hours") {
                error.insertAfter($('#hours-count-holder').parent());
            }
            else if (element.attr("name") == "professionals_count") {
                error.insertAfter($('#professionals-count-holder').parent());
            }
            else if (element.attr("name") == "cleaning_materials") {
                error.insertAfter($('#cleaning-materials-holder').parent());
            }
            else if (element.attr("name") == "instructions") {
                error.insertAfter($('#booking-form textarea[name="instructions"]').parent());
            }
            else if (element.attr("name") == "payment_method") {
                error.insertAfter($('#payment-method-holder').append());
            }
            else {
                error.insertAfter(element);
            }
            //formValidationError(null, error);
        },
        submitHandler: function (form) {
            booking_form = form;
            createBooking();
            return false;
        }
    });
});
/*********************************************************************************************
debug = true;
function logit(data) {
    if (debug == true) {
        console.log(data);
    }
};
var merchantIdentifier = "merchant.com.emaid.elitmaidsUser.livennew";
if (window.ApplePaySession) {
    var promise = ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);
    promise.then(function (canMakePayments) {
        if (canMakePayments) {
            // Display Apple Pay button here.
            logit('hi, I can do ApplePay');
        }
    });
} else {
    logit('ApplePay is not available on this browser');
}
/*********************************************************************************************/
var session = null; // apple pay session store
function createBooking() {
    booking_form = $('#booking-form');
    let payment_method = $('input[name="payment_method"]:checked', booking_form).val();
    if (payment_method == 1) {
        // continue
    }
    else if (payment_method == 2) {
        if (_checkout_token_data) {
            // continue
        }
        else {
            checkoutTokenization();
            return false;
        }
    }
    else if (payment_method == 3) {
        // apple pay
        if (_checkout_token_data) {
            // continue
        }
        else {
            try {
                var runningAmount = _calculation_data.summary.total_payable;
                var runningPP = 0;
                getShippingCosts('domestic_std', true);
                var runningTotal = function () {
                    return runningAmount + runningPP;
                }
                var shippingOption = "";
                var subTotalDescr = "Payment against Cleaning Service";
                function getShippingOptions(shippingCountry) {
                    logit('getShippingOptions: ' + shippingCountry);
                    if (shippingCountry.toUpperCase() == "AE") {
                        shippingOption = [{
                            label: 'Standard Shipping',
                            amount: getShippingCosts('domestic_std', true),
                            detail: '3-5 days',
                            identifier: 'domestic_std'
                        }, {
                            label: 'Expedited Shipping',
                            amount: getShippingCosts('domestic_exp', false),
                            detail: '1-3 days',
                            identifier: 'domestic_exp'
                        }];
                    } else {
                        shippingOption = [{
                            label: 'International Shipping',
                            amount: getShippingCosts('international', true),
                            detail: '5-10 days',
                            identifier: 'international'
                        }];
                    }
                }
                function getShippingCosts(shippingIdentifier, updateRunningPP) {
                    var shippingCost = 0;
                    switch (shippingIdentifier) {
                        case 'domestic_std':
                            shippingCost = 0;
                            break;
                        case 'domestic_exp':
                            shippingCost = 0;
                            break;
                        case 'international':
                            shippingCost = 0;
                            break;
                        default:
                            shippingCost = 0;
                    }
                    if (updateRunningPP == true) {
                        runningPP = shippingCost;
                    }
                    logit('getShippingCosts: ' + shippingIdentifier + " - " + shippingCost + "|" + runningPP);
                    return shippingCost;
                }
                var paymentRequest = {
                    currencyCode: 'AED',
                    countryCode: 'AE',
                    requiredShippingContactFields: ['postalAddress'],
                    lineItems: [{
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        label: 'P&P',
                        amount: runningPP
                    }],
                    total: {
                        label: 'Elitemaids',
                        amount: runningTotal()
                    },
                    supportedNetworks: ['amex', 'masterCard', 'visa'],
                    merchantCapabilities: ['supports3DS', 'supportsCredit', 'supportsDebit']
                };
                session = new ApplePaySession(1, paymentRequest);
                // Merchant Validation
                session.onvalidatemerchant = function (event) {
                    logit(event);
                    var promise = performValidation(event.validationURL);
                    promise.then(function (merchantSession) {
                        session.completeMerchantValidation(merchantSession);
                    });
                }
                function performValidation(valURL) {
                    return new Promise(function (resolve, reject) {
                        var xhr = new XMLHttpRequest();
                        xhr.onload = function () {
                            var data = JSON.parse(this.responseText);
                            logit(data);
                            resolve(data);
                        };
                        xhr.onerror = reject;
                        xhr.open('GET', _base_url + 'applepay/apple_pay_comm.php?u=' + valURL);
                        xhr.send();
                    });
                }
                session.onshippingcontactselected = function (event) {
                    logit('starting session.onshippingcontactselected');
                    logit(
                        'NB: At this stage, apple only reveals the Country, Locality and 4 characters of the PostCode to protect the privacy of what is only a *prospective* customer at this point. This is enough for you to determine shipping costs, but not the full address of the customer.');
                    logit(event);
                    getShippingOptions(event.shippingContact.countryCode);
                    var status = ApplePaySession.STATUS_SUCCESS;
                    var newShippingMethods = shippingOption;
                    var newTotal = {
                        type: 'final',
                        label: 'Elitemaids',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completeShippingContactSelection(status, newShippingMethods, newTotal, newLineItems);
                }
                session.onshippingmethodselected = function (event) {
                    logit('starting session.onshippingmethodselected');
                    logit(event);
                    getShippingCosts(event.shippingMethod.identifier, true);
                    var status = ApplePaySession.STATUS_SUCCESS;
                    var newTotal = {
                        type: 'final',
                        label: 'Elitemaids',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completeShippingMethodSelection(status, newTotal, newLineItems);
                }
                session.onpaymentmethodselected = function (event) {
                    logit('starting session.onpaymentmethodselected');
                    logit(event);
                    var newTotal = {
                        type: 'final',
                        label: 'Elitemaids',
                        amount: runningTotal()
                    };
                    var newLineItems = [{
                        type: 'final',
                        label: subTotalDescr,
                        amount: runningAmount
                    }, {
                        type: 'final',
                        label: 'P&P',
                        amount: runningPP
                    }];
                    session.completePaymentMethodSelection(newTotal, newLineItems);
                }
                session.onpaymentauthorized = function (event) {
                    logit('starting session.onpaymentauthorized');
                    logit(
                        'NB: This is the first stage when you get the *full shipping address* of the customer, in the event.payment.shippingContact object');
                    logit(event.payment.token);
                    var eventToken = JSON.stringify(event.payment.token.paymentData);
                    _checkout_token_data = eventToken;
                    createBooking();
                }
                function sendPaymentToken(paymentToken) {
                    return new Promise(function (resolve, reject) {
                        logit('starting function sendPaymentToken()');
                        logit(
                            "this is where you would pass the payment token to your third-party payment provider to use the token to charge the card. Only if your provider tells you the payment was successful should you return a resolve(true) here. Otherwise reject;");
                        logit(
                            "defaulting to resolve(true) here, just to show what a successfully completed transaction flow looks like");
                        if (debug == true)
                            resolve(true);
                        else
                            reject;
                    });
                }
                session.oncancel = function (event) {
                }
                session.begin();
            }
            catch (err) {
                showTextAlert(JSON.stringify(err.message));
            }
            return false;
        }
    }
    else if (payment_method == 4) {
        booking_btn = $('button[type="button"].gpay-button', booking_form);
        booking_btn.attr('disabled', true);
        if (_checkout_token_data) {
            // continue
        }
        else {
            try {
                const baseRequest = {
                    apiVersion: 2,
                    apiVersionMinor: 0
                };
                const tokenizationSpecification = {
                    type: 'PAYMENT_GATEWAY',
                    parameters: {
                        'gateway': 'checkoutltd',
                        'gatewayMerchantId': _checkout_primary_key
                    }
                };
                const allowedCardNetworks = ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"];

                const allowedCardAuthMethods = ["PAN_ONLY", "CRYPTOGRAM_3DS"];

                const baseCardPaymentMethod = {
                    type: 'CARD',
                    parameters: {
                        allowedAuthMethods: allowedCardAuthMethods,
                        allowedCardNetworks: allowedCardNetworks
                    }
                };
                const cardPaymentMethod = Object.assign(
                    { tokenizationSpecification: tokenizationSpecification },
                    baseCardPaymentMethod
                );

                const paymentsClient = new google.payments.api.PaymentsClient({ environment: _google_pay_environment });

                const isReadyToPayRequest = Object.assign({}, baseRequest);
                isReadyToPayRequest.allowedPaymentMethods = [baseCardPaymentMethod];

                paymentsClient.isReadyToPay(isReadyToPayRequest)
                    .then(function (response) {
                        if (response.result) {
                            // add a Google Pay payment button
                        }
                    })
                    .catch(function (err) {
                        // show error in developer console for debugging
                        console.error(err);
                    });
                const paymentDataRequest = Object.assign({}, baseRequest);
                paymentDataRequest.allowedPaymentMethods = [cardPaymentMethod];
                paymentDataRequest.transactionInfo = {
                    totalPriceStatus: 'NOT_CURRENTLY_KNOWN',
                    //totalPriceStatus: 'FINAL',
                    //totalPrice: _calculation_data.summary.total_payable.toString(),
                    currencyCode: 'AED',
                    countryCode: 'AE'
                };
                paymentDataRequest.merchantInfo = {
                    merchantName: 'Elite Advance Building Cleaning LLC',
                    merchantId: 'BCR2DN4TXLQ3FHS6'
                };

                paymentsClient.loadPaymentData(paymentDataRequest).then(function (paymentData) {
                    // if using gateway tokenization, pass this token without modification
                    paymentToken = paymentData.paymentMethodData.tokenizationData.token;
                    _checkout_token_data = paymentToken;
                    createBooking();
                    // continue
                }).catch(function (err) {
                    // show error in developer console for debugging
                    console.error(err);
                    booking_btn.attr('disabled', false);
                });
                //paymentsClient.prefetchPaymentData(paymentDataRequest);
            }
            catch (err) {
                showTextAlert(err.message);
            }
            return false
        }
    }
    else if (payment_method == 5) {
        // continue
    }
    booking_btn = $('button[type="submit"]', booking_form);
    booking_btn.html(loading_button_html);
    booking_btn.attr('disabled', true);
    create_booking_req = $.ajax({
        type: 'POST',
        url: _base_url + "api/customer/create_booking",
        dataType: 'json',
        data: {
            address_id: $('input[name="address_id"]', booking_form).val(),
            service_type_id: $('input[name="service_type_id"]', booking_form).val(),
            hours: $('input[name="hours"]:checked', booking_form).val(),
            professionals_count: $('input[name="professionals_count"]', booking_form).val(),
            cleaning_materials: $('input[name="cleaning_materials"]:checked', booking_form).val(),
            addons: _cart.addons,
            frequency: $('input[name="frequency"]:checked', booking_form).val(),
            coupon_code: _cart.coupon_code,
            date: $('input[name="date"]', booking_form).val(),
            time: $('input[name="time"]:checked', booking_form).val(),
            payment_method: $('input[name="payment_method"]:checked', booking_form).val(),
            instructions: $('textarea[name="instructions"]', booking_form).val(),
            checkout_token_data: _checkout_token_data
        },
        beforeSend: function () {
            if (create_booking_req != null) {
                create_booking_req.abort();
            }
        },
        success: function (response) {
            if (response.result.status == "success") {
                if (_calculation_data.input.payment_method == 1) {
                    booking_btn.attr('disabled', false);
                    booking_btn.html('Completed');
                    Swal.fire({
                        title: "Booking Received !",
                        html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                        icon: "success",
                        allowOutsideClick: false,
                        confirmButtonText: 'Show Details',
                        timer: 3000,
                    }).then((result) => {
                        if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                            window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                        }
                    });
                }
                else if (_calculation_data.input.payment_method == 2) {
                    let checkout_data = response.result.checkout_data;
                    if (checkout_data.status.toLowerCase() == "pending") {
                        // redirect to bank otp page
                        window.location.href = checkout_data._links.redirect.href;
                    }
                    else if (checkout_data.approved == true) {
                        // payment success
                        booking_btn.html('Completed');
                        Swal.fire({
                            title: "Payment Received !",
                            html: "Booking received with Ref. No. <b>" + response.result.booking_details.booking_reference_id + '</b>',
                            icon: "success",
                            allowOutsideClick: false,
                            confirmButtonText: 'Show Details',
                            timer: 3000,
                        }).then((result) => {
                            if (result.isConfirmed || result.dismiss === swal.DismissReason.timer) {
                                window.location.href = _base_url + 'booking/success/' + response.result.booking_details.booking_reference_id;
                            }
                        });
                    }
                }
            }
            else {
                _checkout_token_data = undefined;
                booking_btn.attr('disabled', false);
                booking_btn.html('Complete');
                create_booking_error(response);
            }
        },
        error: function (response) {
            booking_btn.attr('disabled', false);
            booking_btn.html('Complete');
        },
    });
}
$('.sp-total-price-set').click(function () {
    $('.em-booking-content-right').show(500);
});
$('.sp-sum-rem-btn').click(function () {
    $('.em-booking-content-right').hide(500);
});
$('.show-login-btn').click(function () {
    $('.register-section').hide(500);
    $('.login-section').show(500);
    $('[data-action="step-signup"]').hide();
    $('[data-action="step-login"]').show();
    $("html").animate({ scrollTop: 0 }, "slow");
});
$('.show-register-btn').click(function () {
    $('.login-section').hide(500);
    $('.register-section').show(500);
    $('[data-action="step-signup"]').show();
    $('[data-action="step-login"]').hide();
    $("html").animate({ scrollTop: 0 }, "slow");
});