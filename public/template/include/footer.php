<footer>
  <div class="container">
      <div class="row footer-main m-0">
          <div class="col-lg-12 col-md-12 col-sm-12 footer-copy-right pl-0 pr-0">
              <div class="row ml-0 mr-0">
                  <div class="col-6 footer-left p-0"><p class="copy-right">Emaid © 2022 All Rights Reserved.</p></div>
                  <div class="col-6 footer-right p-0">
                       <div class="design">
                            <a href="http://azinovatechnologies.com/" target="_blank">
                               <div class="azinova-logo">&nbsp;</div>
                            </a>
                            <p class="no-padding">Powered by </p>
                            <div class="clear"></div>
                       </div>
                  </div>
              </div>
          </div>          
      </div>
  </div>
</footer>





<div class="col-sm-12 popup-main otp-popup">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <h5 class="">OTP Verification <span class="em-otp-close-btn"><img src="images/el-close-black.png" title="" style=""></span></h5>
                <p>Enter the OTP you received to<br>
                <span>+971 2345 678</span></p>

                <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                     <div class="row m-0">
                          <div class="col-3 em-text-field-main pl-0 pr-0">
                               <input name="" class="text-field border-right-0" type="text">
                          </div>
                          <div class="col-3 em-text-field-main pl-0 pr-0">
                               <input name="" class="text-field  border-right-0" type="text">
                          </div>
                          <div class="col-3 em-text-field-main pl-0 pr-0">
                               <input name="" class="text-field  border-right-0" type="text">
                          </div>
                          <div class="col-3 em-text-field-main pl-0 pr-0">
                               <input name="" class="text-field" type="text">
                          </div>
                     </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                     <input value="Continue" class="text-field-button show6-step" type="submit">
               </div>      
          </div>
     </div>
</div><!--popup-main end-->





<div class="col-sm-12 popup-main forgot-password-popup">
     <div class="row min-vh-100 d-flex flex-column justify-content-center">
          <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <h5 class="">Forgot Password <span class="em-forgot-close-btn"><img src="images/el-close-black.png" title="" style=""></span></h5>
                <p>Please enter a valid email address with which you created your emaid account.</p>

                <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                     <div class="row m-0">
                          <div class="col-12 em-text-field-main pl-0 pr-0">
                               <input name="" class="text-field" type="text">
                          </div>
                     </div>
                </div>
                
                <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                     <input value="Reset Password" class="text-field-button show6-step" type="submit">
               </div>      
          </div>
     </div>
</div><!--popup-main end-->






<script src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/owl.carousel.js"></script>
<script type="text/javascript" src="js/moment.latest.min.js"></script>
<script type="text/javascript" src="js/pignose.calendar.js"></script>
<script type="text/javascript">
$(function () {
		var couponvall = location.pathname.split('/').pop();
        var tdate = moment().format('YYYY-MM-DD');
        
        $('.toggle-calendar1').pignoseCalendar({
                //format: 'MM/DD YYYY',
                week:6,
                //disabledWeekdays: [5],
                minDate:tdate,
                select: function(date, context) {
                    var dates = date[0] === null ? '' : date[0].format('MMM DD,YYYY');
                    var hourss = $('#select_hours').val();
					$('.mm-loader').show();
                    $.ajax({
                        type: "POST",
                        url: url + 'booking/get_time_availabilty',
                        data: {date_select: dates,hrs: hourss},
                        dataType: 'text',
                        cache: false,
                        success: function (response)
                        {
							$('#hiddenfromtime').val('');
							$(".selectedtime").html('');
                            $('.week-set-box-cont.time1').show();
                            $('#availbletimings').html(response);
                            $('.selecteddate').html(dates);
                            //$('.selectedtime').html('8:00 am');
                            $('#cleaning_date').val(dates);
							price_calculate_new();
                            //$('#hiddenfromtime').val('08:00:00');
                        }

                    });
                    //var selectdate = $(this).date[0];
                    //$('.selecteddate').html(dates);
                    //alert(dates);
                }
	});
});
</script>



<script type="text/javascript">
$(document).ready(function() {
	
	var owl = $("#services-scroll");
	owl.owlCarousel({
	items : 4, //10 items above 1000px browser width
	itemsDesktop : [1000,4], //5 items between 1000px and 901px
	itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
	itemsTablet: [475,2], //2 items between 600 and 0;
	itemsMobile : [320,2], // itemsMobile disabled - inherit from itemsTablet option
	autoPlay : false
    });
	
	
	
	$(".next").click(function(){
       owl.trigger('owl.next');
    });
  
    $(".prev").click(function(){
      owl.trigger('owl.prev');
    });
	
	
	
	$('.sp-total-price-set').click(function(){
		$('.em-booking-content-right').show(500);
	});
	
	
	$('.sp-sum-rem-btn').click(function(){
		$('.em-booking-content-right').hide(500);
	});
	
	
	
	$('.show2-step').click(function(){
		$('.step1').hide(500);
		$('.step2').show(500);
		
		$('.em-head1').hide(0);
		$('.em-head2').show(0);
		$('.date-time-det').show(0);
		$("html").animate({ scrollTop: 0 }, "slow");
		
		
	});
	
	
	$('.show3-step').click(function(){
		$('.step2').hide(500);
		$('.step3').show(500);
		
		$('.em-head2').hide(0);
		$('.em-head3').show(0);
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	
	$('.show4-step').click(function(){
		$('.step3').hide(500);
		$('.step4').show(500);
		
		$('.em-head3').hide(0);
		$('.em-head4').show(0);
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	
	$('.show5-step').click(function(){
		$('.step4').hide(500);
		$('.step5').show(500);
		
		$('.em-head4').hide(0);
		$('.em-head5').show(0);
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	
	$('.show-register-btn').click(function(){
		$('.login-section').hide(500);
		$('.register-section').show(500);
		
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	$('.show-login-btn').click(function(){
		$('.register-section').hide(500);
		$('.login-section').show(500);
		
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	
	
	
	
	$('.show-disinfection-for-office-btn').click(function(){
		$('.disinfection-cleaning-home').hide(500);
		$('.disinfection-cleaning-office').show(500);
	});
	
	$('.show-disinfection-for-home-btn').click(function(){
		$('.disinfection-cleaning-office').hide(500);
		$('.disinfection-cleaning-home').show(500);
	});
	
	
	
	
	
	$('.show-deep-for-office-btn').click(function(){
		$('.deep-cleaning-home').hide(500);
		$('.deep-cleaning-office').show(500);
	});
	
	$('.show-deep-for-home-btn').click(function(){
		$('.deep-cleaning-office').hide(500);
		$('.deep-cleaning-home').show(500);
	});
	
	
	$('.show-L-sofa-btn').click(function(){
		$('.normal-sofa').hide(500);
		$('.L-shape-sofa').show(500);
	});
	
	
	$('.show-normal-sofa-btn').click(function(){
		$('.normal-sofa').show(500);
		$('.L-shape-sofa').hide(500);
	});
	
	
	$('.em-otp-close-btn').click(function(){
		$('.otp-popup').hide(500);
	});
	
	
	$('.em-forgot-close-btn').click(function(){
		$('.forgot-password-popup').hide(500);
	});
	
	
	
	
	
	
	$('.back-to1-btn').click(function(){
		$('.step1').show(500);
		$('.step2').hide(500);
		
		$('.em-head1').show(0);
		$('.em-head2').hide(0);
		$('.date-time-det').hide(0);
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	$('.back-to2-btn').click(function(){
		$('.step2').show(500);
		$('.step3').hide(500);
		
		$('.em-head2').show(0);
		$('.em-head3').hide(0);
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	
	$('.back-to3-btn').click(function(){
		$('.step3').show(500);
		$('.step4').hide(500);
		
		$('.em-head3').show(0);
		$('.em-head4').hide(0);
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	
	$('.back-to4-btn').click(function(){
		$('.step4').show(500);
		$('.step5').hide(500);
		
		$('.em-head4').show(0);
		$('.em-head5').hide(0);
		$("html").animate({ scrollTop: 0 }, "slow");
	});
	
	
	
	 
	 
	 
	 
	 
	 
	
	
<!--pro-but8 pro-but-->
	
	
	
$(".ser-cont").each(function(){
       //$(this).hide(0);
    if($(this).attr('id') == 'cont1') {
      // $(this).show(0);
	   
    }
});

$('.ser-but').on( "click", function(e) {
    e.preventDefault();
	    $('.ser-but').removeClass('active');
	    $(this).addClass('active');
    var id = $(this).attr('data-id'); 
    $(".ser-cont").each(function(){
           $(this).hide(500);
        if($(this).attr('id') == id) {
           $(this).show(500);
        }
    });
});


	
	
});
</script>







<script type="text/javascript">
			var images = new Array()
			function preload() {
				for (i = 0; i < preload.arguments.length; i++) {
					images[i] = new Image()
					images[i].src = preload.arguments[i]
				}
			}
			preload(
			    "images/logo.png",
				"images/el-close.png",
				"images/el-close-black.png",
				"images/el-close-red.png",
				"images/el-close-white.png",
				"images/el-downarrow.png",
				"images/el-leftarrow.png",
				"images/el-leftarrow-green.png",
				"images/el-leftarrow-white.png",
				"images/el-minus.png",
				"images/el-minus-white.png",
				"images/el-plus.png",
				"images/el-plus-white.png",
				"images/el-rightarrow.png",
				"images/el-rightarrow-green.png",
				"images/el-rightarrow-white.png",
				"images/el-tick.png",
				"images/el-tick-green.png",
				"images/el-tick-white.png",
				"images/el-tick-violet.png",
				"images/el-uparrow.png",
				"images/el-uparrow-black.png",
				"images/el-uparrow-white.png",
				"images/em-icon1.png",
				"images/em-icon2.png",
				"images/em-icon3.png",
				"images/em-icon4.png",
				"images/em-icon5.png",
				"images/em-icon6.png",
				"images/em-icon7.png",
				"images/em-icon8.png",
				"images/em-icon9.png",
				"images/em-icon10.png",
				"images/em-icon11.png",
				"images/em-icon20.png",
				"images/em-icon21.png",
				"images/login.png",
				"images/register.png",
				"images/el-add1.png",
				"images/el-add1-violet.png",
				"images/el-add2.png",
				"images/el-add2-violet.png",
				"images/el-add3.png",
				"images/el-add3-violet.png",
				"images/uae-flag.jpg"
				

			)
</script>



