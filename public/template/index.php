<!doctype html>
<html lang="en">
<head>
    <title>Emaid Bookig</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="images/favicon.png"/>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/pignose.calendar.css"/>
    
</head>
<body class="bg-color">

<header>
   <div class="container">	
      <div class="row m-0">
      
          <div class="col-lg-2 col-md-12 col-sm-12 pl-0 pr-0">
          
               <div class="em-back-arrow" title="Previous Step"></div>
               
               
               <div class="logo"><a href="index.php"><img src="images/emaid-logo.png" alt=""></a></div>
               <div class="em-mob-icons">
                    <ul>
                        <li class="mob-home"><i class="fa fa-user"></i></li>
                        <li class="mob-home"><i class="fa fa-home"></i></li>
                    </ul>
               </div>
               
               <div class="clear"></div> 
          </div>
          
          
          <div class="col-lg-10 col-sm-12  menu pl-0 pr-0">
                    <nav id="primary_nav_wrap">
                        <ul> 
                            <li class="booking"><a href="index.php">Booking</a>|</li>
                            <li class="log-reg"><a href="#" class="pr-0">Login</a> / <a href="#" class="pl-0 pr-0">Register</a></li>
                            <div class="clear"></div>
                         </ul>
                         <div class="clear"></div>   
                     </nav>
                    <div class="clear"></div>
              
             
          </div>
      
      </div>
  </div>
</header><!--Header Section-->






<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
      <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0">
          
          
          
               <div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">

                    <div class="col-md-12 col-sm-12 em-step-head em-head1 no-left-right-padding">
                         <h2>Book Your Service</h2>
                         <ul>
                             <li class="active"><span></span></li>
                             <li><span></span></li>
                             <li><span></span></li>
                             <li><span></span></li>
                             <li><span></span></li>
                         </ul>
                    </div>
                    
                    
                    
                    <div class="col-md-12 col-sm-12 em-step-head em-head2 no-left-right-padding">
                         <h2>Date and Time</h2>
                         <ul>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li><span></span></li>
                             <li><span></span></li>
                             <li><span></span></li>
                         </ul>
                    </div>
                    
                    
                    
                    
                    <div class="col-md-12 col-sm-12 em-step-head em-head3 no-left-right-padding">
                         <h2>Login / Register</h2>
                         <ul>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li><span></span></li>
                             <li><span></span></li>
                         </ul>
                    </div>
                    
                    
                    
                    
                    <div class="col-md-12 col-sm-12 em-step-head em-head4 no-left-right-padding">
                         <h2>Your Location</h2>
                         <ul>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li><span></span></li>
                         </ul>
                    </div>
                    
                    
                    
                    
                    <div class="col-md-12 col-sm-12 em-step-head em-head5 no-left-right-padding">
                         <h2>Payment Details</h2>
                         <ul>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                             <li class="active"><span></span></li>
                         </ul>
                    </div>
                    
               </div>
               
               
               
               
               
               
               <div class="col-12 em-booking-content-set pl-0 pr-0">
               	    <div class="row em-booking-content-set-main ml-0 mr-0">
                         <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">
                              
                              
                              <div class="col-12 step1 pl-0 pr-0">
                                   <div id="services-scroll" class="owl-carousel em-ser-scroll-thumb-main">
                                                                            
                                         <div class="item">
                                               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                                                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="images/em-icon2.png" alt=""></div>
                                                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                                                         <p>House Cleaning<br>Service</p>
                                                         <input value="BOOK NOW" class="text-field-button ser-but1 ser-but active" data-id="cont1" type="submit">
                                                    </div>
                                               </div>
                                         </div>
                                         
                                         
                                         
                                         <div class="item">
                                               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                                                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="images/em-icon1.png" alt=""></div>
                                                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                                                         <p>Disinfection<br>Service</p>
                                                         <input value="BOOK NOW" class="text-field-button ser-but2 ser-but" data-id="cont2" type="submit">
                                                    </div>
                                               </div>
                                         </div>
                                         
                                         
                                         
                                         <div class="item">
                                               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                                                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="images/em-icon10.png" alt=""></div>
                                                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                                                         <p>Deep Cleaning<br>Services</p>
                                                         <input value="BOOK NOW" class="text-field-button ser-but3 ser-but" data-id="cont3" type="submit">
                                                    </div>
                                               </div>
                                         </div>
                                         

                                         
                                         <div class="item">
                                               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                                                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="images/em-icon6.png" alt=""></div>
                                                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                                                         <p>Carpet Cleaning<br>services</p>
                                                         <input value="BOOK NOW" class="text-field-button ser-but4 ser-but" data-id="cont4" type="submit">
                                                    </div>
                                               </div>
                                         </div>
                                         
                                         
                                         
                                         <div class="item">
                                               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                                                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="images/em-icon4.png" alt=""></div>
                                                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                                                         <p>sofa cleaning<br>services</p>
                                                         <input value="BOOK NOW" class="text-field-button ser-but5 ser-but" data-id="cont5" type="submit">
                                                    </div>
                                               </div>
                                         </div>
                                         
                                         
                                         
                                         <div class="item">
                                               <div class="col-12 em-ser-scroll-thumb em-booking-det-cont">
                                                    <div class="col-12 em-ser-srl-tmb-image pl-0 pr-0"><img src="images/em-icon11.png" alt=""></div>
                                                    <div class="col-12 em-ser-srl-tmb-cont pl-0 pr-0">
                                                         <p>Mattress<br>Cleaning</p>
                                                         <input value="BOOK NOW" class="text-field-button ser-but6 ser-but" data-id="cont6" type="submit">
                                                    </div>
                                               </div>
                                         </div>
                                         
                                   </div>
                               
                         
                                   <div class="col-12 em-booking-det-cont book-det-cont-set pl-0 pr-0">
                                  
                                       <div class="col-12 book-det-cont-set-main house-cleaning-service ser-cont1 ser-cont" id="cont1">
                                            <h5>House Cleaning Services</h5>
                                             
                                             
                                             <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                  
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>Number Of Hours</p>
                                                            <div class="col-12 em-text-field-main number-of-hours em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>1</li>
                                                                     <li>2</li>
                                                                     <li class="selected">3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                     <li>6</li>
                                                                     <li>7</li>
                                                                     <li>8</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How many maids</p>
                                                            <div class="col-12 em-text-field-main how-many-maids pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li class="em-minus">&nbsp;</li>
                                                                     <li class="num-maids"><input name="" class="text-field" type="text" placeholder="1"></li>
                                                                     <li class="em-plus">&nbsp;</li>
                                                                 </ul>
                                                           </div>
                                                       </div>
                                              
                                                 </div> 
                                             </div>
                                             
                                             
                                             
                                             
                                             <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                  
                                                       <!--<div class="col-sm-6 em-field-main">
                                                            <p>Number of Visits</p>
                                                            <div class="col-12 em-text-field-main number-of-visits em-box-1-5 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>ONE VISIT</li>
                                                                     <li>2</li>
                                                                     <li class="selected">3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                 </ul>
                                                           </div>
                                                       </div>-->
                                                       
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>Do you want cleaning materials ?  (AED 10/hr)</p>
                                                            <div class="col-12 em-text-field-main cleaning-materials em-box-2 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>YES, PLEASE</li>
                                                                     <li class="selected">NO, I HAVE THEM</li>
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                              
                                                 </div> 
                                             </div>
                                            
                                            
                                            
                                            
                                             <div class="col-sm-12 em-extra-ser-set">
                                               <p>Extra Services</p>
                                               <div class="row">
                                               
                                                     <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                          <div class="row ml-0 mr-0 em-extra-ser-tmb">
                                                               <div class="col-sm-4 em-extra-ser-img"><img src="images/em-icon9.png" alt=""></div>
                                                               <div class="col-sm-8 em-extra-ser-cont">
                                                                    Interior Windows<br>
                                                                    <span>60 min 0 AED</span> 
                                                               </div>
                                                          </div>
                                                     </div>
                                                     
                                                     
                                                     <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                          <div class="row ml-0 mr-0 em-extra-ser-tmb selected">
                                                               <div class="col-sm-4 em-extra-ser-img"><img src="images/em-icon5.png" alt=""></div>
                                                               <div class="col-sm-8 em-extra-ser-cont">
                                                                    Ironing<br>
                                                                    <span>30 min 0 AED</span> 
                                                               </div>
                                                          </div>
                                                     </div>
                                                     
                                                     
                                                     <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                          <div class="row ml-0 mr-0 em-extra-ser-tmb">
                                                               <div class="col-sm-4 em-extra-ser-img"><img src="images/em-icon8.png" alt=""></div>
                                                               <div class="col-sm-8 em-extra-ser-cont">
                                                                    Oven Cleaning<br>
                                                                    <span>30 min 0 AED</span> 
                                                               </div>
                                                          </div>
                                                     </div>
                                                     
                                                     
                                                     <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                          <div class="row ml-0 mr-0 em-extra-ser-tmb">
                                                               <div class="col-sm-4 em-extra-ser-img"><img src="images/em-icon7.png" alt=""></div>
                                                               <div class="col-sm-8 em-extra-ser-cont">
                                                                    Fridge cleaning<br>
                                                                    <span>30 min 0 AED</span> 
                                                               </div>
                                                          </div>
                                                     </div>
                                                     
                                               </div>
                                               
                                          </div> 
                                       
                                      
                                       </div><!--house-cleaning-service end-->
                                       
                                       
                                       
                                       
                                       <div class="col-12 book-det-cont-set-main disinfection-cleaning-service  ser-cont2 ser-cont" id="cont2">
                                            <h5>Disinfection Services</h5>
                                            
                                            <div class="col-sm-12 disinfection-cleaning-home pl-0 pr-0">
                                                 <div class="col-sm-12 em-field-main-set"> 
                                                      <div class="row m-0">
                                                           
                                                           <div class="col-sm-6 em-field-main">
                                                                <p>Disinfection Service for</p>
                                                                <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                     <ul class="clearfix">
                                                                         <li class="selected">HOME</li>
                                                                         <li class="show-disinfection-for-office-btn">OFFICE</li>
                                                                     </ul>
                                                               </div>
                                                           </div>
                                                           
                                                           
                                                           <div class="col-sm-6 em-field-main how-often em-width-auto">
                                                                <p>What is your building type?</p>
                                                                <ul>
                                                                       <li>
                                                                           <input id="building-type1" value="cash" name="building-type" class="" checked="" type="radio">
                                                                           <label for="building-type1"> <span></span> &nbsp; Apartment</label>
                                                                       </li>
                                                                       
                                                                       
                                                                       <li>
                                                                           <input id="building-type2" value="card" name="building-type" class="" type="radio">
                                                                           <label for="building-type2"> <span></span> &nbsp; Villa</label>
                                                                       </li>
                                                                 </ul>
                                                            </div>
                                                            
                                                  
                                                     </div> 
                                                 </div>
                                             
                                                 <div class="col-sm-12 em-often-section-box pt-1">
                                                      <div class="row m-0">
                                                      
                                                      <div class="col-sm-6 em-field-main">
                                                                <p>How many bedroom home you have?</p>
                                                                <div class="col-12 em-text-field-main em-box-1-5 how-many-bedroom pl-0 pr-0">
                                                                     <ul class="clearfix">
                                                                         <li>STUDIO</li>
                                                                         <li>1</li>
                                                                         <li>2</li>
                                                                         <li class="selected">3</li>
                                                                         <li>4</li>
                                                                         <li>5</li>
                                                                     </ul>
                                                               </div>
                                                           </div>
                                                           
                                                      
                                                      <div class="col-sm-6 em-field-main">
                                                            <p>How many...( when we select villa)</p>
                                                            <div class="col-12 em-text-field-main number-of-hours em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>2</li>
                                                                     <li class="selected">3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                     <li>6</li>
                                                                     <li>7</li>
                                                                     <li>8</li>
                                                                     <li>9</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>     
                                                           
                                                      </div>     
                                                 </div>
                                            </div><!--disinfection-cleaning-home end-->
                                            
                                            
                                            <div class="col-sm-12 disinfection-cleaning-office pl-0 pr-0">
                                                 <div class="col-sm-12 em-field-main-set"> 
                                                      <div class="row m-0">
                                                           
                                                           <div class="col-sm-6 em-field-main">
                                                                <p>Disinfection Service for</p>
                                                                <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                     <ul class="clearfix">
                                                                         <li class="show-disinfection-for-home-btn">HOME</li>
                                                                         <li class="selected">OFFICE</li>
                                                                     </ul>
                                                               </div>
                                                           </div>
                                                           
                                                  
                                                     </div> 
                                                 </div>
                                                 
                                                 <div class="col-sm-12 em-time-section-box pt-4">
                                                     <div class="col-sm-12 time-set">
                                                          <ul>
                                                              <p>What is the size of your office?</p>
                                                              
                                                              <li class="selected">
                                                                  <div class="tick-mark">&nbsp;</div>
                                                                  <div class="tick-text">Upto 600 sq ft</div>
                                                                  <div class="clear"></div>
                                                              </li>
                                                              
                                                              <li>
                                                                  <div class="tick-mark">&nbsp;</div>
                                                                  <div class="tick-text">601 sq ft – 900 sq ft </div>
                                                                  <div class="clear"></div>
                                                              </li>
                                                              
                                                              <li>
                                                                  <div class="tick-mark">&nbsp;</div>
                                                                  <div class="tick-text">901 sq ft – 1300 sq ft </div>
                                                                  <div class="clear"></div>
                                                              </li>
                                                              
                                                              <li>
                                                                  <div class="tick-mark">&nbsp;</div>
                                                                  <div class="tick-text">1301 sq ft – 1800 sq ft </div>
                                                                  <div class="clear"></div>
                                                              </li>
                                                              
                                                              <li>
                                                                  <div class="tick-mark">&nbsp;</div>
                                                                  <div class="tick-text">1801 sq ft – 2500 sq ft </div>
                                                                  <div class="clear"></div>
                                                              </li>
                                                              
                                                              <div class="clear"></div>
                                                           </ul>
                                                     </div>
                                                 </div>
                                            </div><!--disinfection-cleaning-office end-->
                                            
                                       </div><!--disinfection-cleaning-service end-->
                                       
                                       
                                       
                                       
                                       <div class="col-sm-12 book-det-cont-set-main deep-cleaning-service  ser-cont3 ser-cont" id="cont3">
                                            <h5>Deep Cleaning Services</h5>
                                             
                                             <div class="col-sm-12 deep-cleaning-home pl-0 pr-0">
                                                  <div class="col-sm-12 em-field-main-set"> 
                                                            <div class="row m-0">
                                                                 
                                                                 <div class="col-sm-6 em-field-main">
                                                                      <p>Deep cleaning for</p>
                                                                      <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                           <ul class="clearfix">
                                                                               <li class="selected">HOME</li>
                                                                               <li class="show-deep-for-office-btn">OFFICE</li>
                                                                           </ul>
                                                                     </div>
                                                                 </div>
                                                                 
                                                                 
                                                                 <div class="col-sm-6 em-field-main">
                                                                      <p>Deep cleaning for</p>
                                                                      <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                           <ul class="clearfix">
                                                                               <li class="selected">Furnished</li>
                                                                               <li class="">Unfurnished</li>
                                                                           </ul>
                                                                     </div>
                                                                 </div>
                                                                 
                                                                 
                                                                 
                                                                  
                                                        
                                                           </div> 
                                                       </div>
                                             
                                             
                                                   <div class="col-sm-12 em-often-section-box pt-1">
                                                       <div class="col-sm-12 how-often">
                                                          <p>What is your building type?</p>
                                                          <ul>
                                                                 <li>
                                                                     <input id="building-type3" value="cash" name="building" class="" checked="" type="radio">
                                                                     <label for="building-type3"> <span></span> &nbsp; Apartment</label>
                                                                 </li>
                                                                 
                                                                 
                                                                 <li>
                                                                     <input id="building-type4" value="card" name="building" class="" type="radio">
                                                                     <label for="building-type4"> <span></span> &nbsp; Villa</label>
                                                                 </li>
                                                           </ul>
                                                        </div>
                                                   </div>
                                                   
                                                   
                                                   <div class="col-sm-12 em-field-main-set"> 
                                                        <div class="row m-0">
                                                        
                                                             <div class="col-sm-6 em-field-main">
                                                                      <p>How many bedroom home you have?</p>
                                                                      <div class="col-12 em-text-field-main em-box-1-5 how-many-bedroom pl-0 pr-0">
                                                                           <ul class="clearfix">
                                                                               <li>STUDIO</li>
                                                                               <li>1</li>
                                                                               <li>2</li>
                                                                               <li class="selected">3</li>
                                                                               <li>4</li>
                                                                               <li>5</li>
                                                                           </ul>
                                                                     </div>
                                                                 </div>
                                                                 
                                                                 
                                                                 
                                                             
                                                             <div class="col-sm-6 em-field-main">
                                                                  <p>Do you require Floor scrubbing and polishing?</p>
                                                                  <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                       <ul class="clearfix">
                                                                           <li>NO</li>
                                                                           <li class="selected">YES, PLEASE</li>
                                                                       </ul>
                                                                 </div>
                                                             </div>
                                                             
                                                             
                                                             
                                                              
                                                    
                                                       </div> 
                                                   </div> 
                                             </div><!--deep-cleaning-home end-->
                                             
                                             
                                             <div class="col-sm-12 deep-cleaning-office pl-0 pr-0">
                                                       
                                                       <div class="col-sm-12 em-field-main-set"> 
                                                            <div class="row m-0">
                                                            
                                                                 <div class="col-sm-6 em-field-main">
                                                                      <p>Deep cleaning for</p>
                                                                      <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                           <ul class="clearfix">
                                                                               <li class="show-deep-for-home-btn">HOME</li>
                                                                               <li class="selected">OFFICE</li>
                                                                           </ul>
                                                                     </div>
                                                                 </div>
                                                                 
                                                                 
                                                                 <div class="col-sm-6 em-field-main">
                                                                      <p>Deep cleaning for</p>
                                                                      <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                           <ul class="clearfix">
                                                                               <li class="selected">Furnished</li>
                                                                               <li class="">Unfurnished</li>
                                                                           </ul>
                                                                     </div>
                                                                 </div>
                                                                 
                                                       </div> 
                                                       </div>
                                                       
                                                       <div class="col-sm-12 em-field-main-set"> 
                                                            <div class="row m-0">          
                                                                 <div class="col-sm-6 em-field-main">
                                                                      <p>Do you require Floor scrubbing and polishing?</p>
                                                                      <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                           <ul class="clearfix">
                                                                               <li>NO</li>
                                                                               <li class="selected">YES, PLEASE</li>
                                                                           </ul>
                                                                     </div>
                                                                 </div>
                                                                 
                                                                 
                                                                 
                                                                  
                                                        
                                                           </div> 
                                                       </div>
                                             
                                             
                                                       <div class="col-sm-12 em-time-section-box pt-4">
                                                           <div class="col-sm-12 time-set">
                                                                <ul>
                                                                    <p>What is the size of your office?</p>
                                                                    
                                                                    <li class="selected">
                                                                        <div class="tick-mark">&nbsp;</div>
                                                                        <div class="tick-text">Upto 600 sq ft</div>
                                                                        <div class="clear"></div>
                                                                    </li>
                                                                    
                                                                    <li>
                                                                        <div class="tick-mark">&nbsp;</div>
                                                                        <div class="tick-text">601 sq ft – 900 sq ft </div>
                                                                        <div class="clear"></div>
                                                                    </li>
                                                                    
                                                                    <li>
                                                                        <div class="tick-mark">&nbsp;</div>
                                                                        <div class="tick-text">901 sq ft – 1300 sq ft </div>
                                                                        <div class="clear"></div>
                                                                    </li>
                                                                    
                                                                    <li>
                                                                        <div class="tick-mark">&nbsp;</div>
                                                                        <div class="tick-text">1301 sq ft – 1800 sq ft </div>
                                                                        <div class="clear"></div>
                                                                    </li>
                                                                    
                                                                    <li>
                                                                        <div class="tick-mark">&nbsp;</div>
                                                                        <div class="tick-text">1801 sq ft – 2500 sq ft </div>
                                                                        <div class="clear"></div>
                                                                    </li>
                                                                    
                                                                    <div class="clear"></div>
                                                                 </ul>
                                                           </div>
                                                       </div>
                                              
                                             </div><!--deep-cleaning-office end-->
                                                
                                       </div><!--deep-cleaning-service end-->
                                       
                                       
                                       
                                       
                                       <div class="col-12 book-det-cont-set-main carpet-cleaning-service  ser-cont4 ser-cont" id="cont4">
                                            <h5>Carpet Cleaning Services</h5>
                                            
                                             
                                             <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                  
                                                       <div class="col-sm-12 em-often-section-box pt-1 pl-0 pr-0">
                                                         <div class="col-sm-12 how-often">
                                                            <p>What is your carpet size?</p>
                                                            <ul>
                                                                   <li>
                                                                       <input id="carpet1" value="cash" name="carpet" class="" checked="" type="checkbox">
                                                                       <label for="carpet1"> <span></span> &nbsp; 5 x 10 ft</label>
                                                                   </li>
                                                                   
                                                                   
                                                                   <li>
                                                                       <input id="carpet2" value="card" name="carpet" class="" type="checkbox">
                                                                       <label for="carpet2"> <span></span> &nbsp; 8 x 12 ft</label>
                                                                   </li>
                                                                   
                                                                   
                                                                   <li>
                                                                       <input id="carpet3" value="card" name="carpet" class="" type="checkbox">
                                                                       <label for="carpet3"> <span></span> &nbsp; 10 x 15 ft</label>
                                                                   </li>
                                                             </ul>
                                                          </div>
                                                     </div>
                                                       
                                                       
                                                       
                                              
                                                 </div> 
                                             </div>
                                             
                                             <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                  
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How many <span>5 x 10 ft</span> carpet will be cleaned?</p>
                                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>1</li>
                                                                     <li>2</li>
                                                                     <li class="selected">3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                     <li>6</li>
                                                                     <li>7</li>
                                                                     <li>8</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How many <span>8 x 12 ft</span> carpet will be cleaned?</p>
                                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>1</li>
                                                                     <li>2</li>
                                                                     <li>3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                     <li>6</li>
                                                                     <li class="selected">7</li>
                                                                     <li>8</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How many <span>10 x 15 ft</span> carpet will be cleaned?</p>
                                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>1</li>
                                                                     <li class="selected">2</li>
                                                                     <li>3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                     <li>6</li>
                                                                     <li>7</li>
                                                                     <li>8</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                              
                                                 </div> 
                                             </div>
                                             
                                             
                                       </div><!--carpet-cleaning-service end-->
                                       
                                       
                                       
                                       
                                       <div class="col-12 book-det-cont-set-main sofa-cleaning-service  ser-cont5 ser-cont" id="cont5">
                                            <h5>Sofa Cleaning Services</h5>
                                               <div class="col-sm-12 normal-sofa pl-0 pr-0">
                                                    <div class="col-sm-12 em-field-main-set"> 
                                                        <div class="row m-0">
                                                        
                                                             
                                                             <div class="col-sm-6 em-field-main">
                                                                  <p>Sofa type?</p>
                                                                  <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                       <ul class="clearfix">
                                                                           <li class="selected">NORMAL SOFA</li>
                                                                           <li class="show-L-sofa-btn">L SHAPE SOFA</li>
                                                                       </ul>
                                                                 </div>
                                                             </div>
                                                             
                                                             
                                                             <div class="col-sm-6 em-field-main pr-0 em-width-auto how-often">
                                                                  <p>What is your seat type?</p>
                                                                  <ul>
                                                                         <li>
                                                                             <input id="seat1" value="cash" name="seat" class="" checked="" type="checkbox">
                                                                             <label for="seat1"> <span></span> &nbsp; 3 Seat Sofa</label>
                                                                         </li>
                                                                         
                                                                         
                                                                         <li>
                                                                             <input id="seat2" value="card" name="seat" class="" type="checkbox">
                                                                             <label for="seat2"> <span></span> &nbsp; 4 Seat Sofa</label>
                                                                         </li>
                                                                         
                                                                         
                                                                        
                                                                        
                                                                   </ul>
                                                                </div>
                                                             
                                                             
                                                             
                                                             
                                                      
                                                          </div> 
                                                   </div>
                                                   
                                                   
                                                      <div class="col-sm-12 em-field-main-set"> 
                                                        <div class="row m-0">       
                                                             
                                                             <div class="col-sm-6 em-field-main">
                                                                  <p>How many <span>3 seats sofa</span> will be cleaned?</p>
                                                                  <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                       <ul class="clearfix">
                                                                           <li>1</li>
                                                                           <li>2</li>
                                                                           <li class="selected">3</li>
                                                                           <li>4</li>
                                                                           <li>5</li>
                                                                           <li>6</li>
                                                                           <li>7</li>
                                                                           <li>8</li>
                                                                           
                                                                       </ul>
                                                                 </div>
                                                             </div>
                                                             
                                                             
                                                             <div class="col-sm-6 em-field-main">
                                                                  <p>How many <span>4 seats sofa</span> sofa will be cleaned?</p>
                                                                  <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                       <ul class="clearfix">
                                                                           <li>1</li>
                                                                           <li>2</li>
                                                                           <li >3</li>
                                                                           <li>4</li>
                                                                           <li>5</li>
                                                                           <li class="selected">6</li>
                                                                           <li>7</li>
                                                                           <li>8</li>
                                                                           
                                                                       </ul>
                                                                 </div>
                                                             </div>
                                                             
                                                             
                                                             
                                                             
                                                    
                                                       </div> 
                                                   </div>
                                                    
                                               </div><!--soafa-cleaning-home end-->
                                               
                                               
                                               
                                               <div class="col-sm-12 L-shape-sofa pl-0 pr-0">
                                                    <div class="col-sm-12 em-field-main-set"> 
                                                        <div class="row m-0">
                                                        
                                                             <div class="col-sm-12 em-field-main-set"> 
                                                        <div class="row m-0">
                                                        
                                                             
                                                             <div class="col-sm-6 em-field-main">
                                                                  <p>Sofa type?</p>
                                                                  <div class="col-12 em-text-field-main em-box-2 pl-0 pr-0">
                                                                       <ul class="clearfix">
                                                                           <li class="show-normal-sofa-btn">NORMAL SOFA</li>
                                                                           <li class="selected">L SHAPE SOFA</li>
                                                                       </ul>
                                                                 </div>
                                                             </div>
                                                             
                                                             
                                                             <div class="col-sm-6 em-field-main pr-0 em-width-auto how-often">
                                                                  <p>What is your seat type?</p>
                                                                  <ul>
                                                                         <li>
                                                                             <input id="seat1" value="cash" name="seat" class=""type="checkbox">
                                                                             <label for="seat1"> <span></span> &nbsp; 3 Seat L Sofa</label>
                                                                         </li>
                                                                         
                                                                         
                                                                         <li>
                                                                             <input id="seat2" value="card" name="seat" class="" checked=""  type="checkbox">
                                                                             <label for="seat2"> <span></span> &nbsp; 4 Seat L Sofa</label>
                                                                         </li>
                                                                         
                                                                         
                                                                        
                                                                        
                                                                   </ul>
                                                                </div>
                                                             
                                                             
                                                             
                                                             
                                                      
                                                          </div> 
                                                   </div>
                                                   
                                                   
                                                      <div class="col-sm-12 em-field-main-set"> 
                                                        <div class="row m-0">       
                                                             
                                                             <div class="col-sm-6 em-field-main">
                                                                  <p>How many <span> 3 seats sofa</span> will be cleaned?</p>
                                                                  <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                       <ul class="clearfix">
                                                                           <li>1</li>
                                                                           <li>2</li>
                                                                           <li>3</li>
                                                                           <li>4</li>
                                                                           <li>5</li>
                                                                           <li>6</li>
                                                                           <li class="selected">7</li>
                                                                           <li>8</li>
                                                                           
                                                                       </ul>
                                                                 </div>
                                                             </div>
                                                             
                                                             
                                                             <div class="col-sm-6 em-field-main">
                                                                  <p>How many <span>4 seats sofa</span> will be cleaned?</p>
                                                                  <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                       <ul class="clearfix">
                                                                           <li>1</li>
                                                                           <li class="selected">2</li>
                                                                           <li >3</li>
                                                                           <li>4</li>
                                                                           <li>5</li>
                                                                           <li>6</li>
                                                                           <li>7</li>
                                                                           <li>8</li>
                                                                           
                                                                       </ul>
                                                                 </div>
                                                             </div> 
                                                             
                                                    
                                                       </div> 
                                                   </div>
                                                    
                                                       </div> 
                                                   </div>
                                               </div><!--soafa-cleaning-office end-->
                                             
                                             
                                             
                                       </div><!--sofa-cleaning-service end-->
                                       
                                       
                                       
                                       
                                       <div class="col-12 book-det-cont-set-main mattress-cleaning-service  ser-cont6 ser-cont" id="cont6">
                                            <h5>Mattress Cleaning Services</h5>
                                             
                                                 <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                  
                                                       <div class="col-sm-12 em-often-section-box pt-1 pl-0 pr-0">
                                                         <div class="col-sm-12 how-often">
                                                            <p>What is your mattress size?</p>
                                                            <ul>
                                                                   <li>
                                                                       <input id="mattress1" value="cash" name="mattress" class="" checked="" type="checkbox">
                                                                       <label for="mattress1"> <span></span> &nbsp; Single (90 x 190 cm) </label>
                                                                   </li>
                                                                   
                                                                   
                                                                   <li>
                                                                       <input id="mattress2" value="card" name="mattress" class="" type="checkbox">
                                                                       <label for="mattress2"> <span></span> &nbsp; Queen (135 x 190 cm) </label>
                                                                   </li>
                                                                   
                                                                   
                                                                   <li>
                                                                       <input id="mattress3" value="card" name="mattress" class="" type="checkbox">
                                                                       <label for="mattress3"> <span></span> &nbsp; King (150 x 200 cm) </label>
                                                                   </li>
                                                                   
                                                                   <li>
                                                                       <input id="mattress4" value="card" name="mattress" class="" type="checkbox">
                                                                       <label for="mattress4"> <span></span> &nbsp; Double King (180 x 200 cm) </label>
                                                                   </li>
                                                             </ul>
                                                          </div>
                                                     </div>
                                                     
                                                   
                                                       </div> 
                                             </div>
                                                   
                                                   
                                                   <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">  
                                                     
                                                     
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How many <span>Single mattress</span> will be cleaned?</p>
                                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>1</li>
                                                                     <li>2</li>
                                                                     <li class="selected">3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                     <li>6</li>
                                                                     <li>7</li>
                                                                     <li>8</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How many <span>Queen mattress</span> will be cleaned?</p>
                                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>1</li>
                                                                     <li class="selected">2</li>
                                                                     <li>3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                     <li>6</li>
                                                                     <li>7</li>
                                                                     <li>8</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How many <span>King mattress</span> will be cleaned?</p>
                                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>1</li>
                                                                     <li>2</li>
                                                                     <li>3</li>
                                                                     <li>4</li>
                                                                     <li>5</li>
                                                                     <li>6</li>
                                                                     <li class="selected">7</li>
                                                                     <li>8</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How many <span>Double King mattress</span> will be cleaned?</p>
                                                            <div class="col-12 em-text-field-main em-box-8 pl-0 pr-0">
                                                                 <ul class="clearfix">
                                                                     <li>1</li>
                                                                     <li>2</li>
                                                                     <li>3</li>
                                                                     <li>4</li>
                                                                     <li class="selected">5</li>
                                                                     <li>6</li>
                                                                     <li>7</li>
                                                                     <li>8</li>
                                                                     
                                                                 </ul>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                              
                                                 </div> 
                                             </div>
                                       </div><!--mattress-cleaning-service end-->
                                       
                                  </div>
                              
                              
                                   <div class="col-12 em-booking-det-cont em-next-btn">
                                       <div class="row em-next-btn-set ml-0 mr-0">
                                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                                 <!--<span class="em-back-arrow " title="Previous Step"></span>-->
                                                 
                                                 <div class="col-12 sp-total-price-set pl-0 pr-0">
                                                      Total<br>
                                                      <span>AED 380</span> 
                                                 </div>
                                           
                                            </div>
                                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                                 <input value="Next" class="text-field-button show2-step" type="submit">
                                            </div>
                                       </div>
                                  </div>
                              
                               </div><!--step1 end-->
                              
                              
                               
                               
                               
                               
                               <div class="col-12 step2 pl-0 pr-0">
                                    <div class="col-12 em-booking-det-cont book-det-cont-set ">
                                         
                                    <div class="col-lg-12 col-md-12 col-sm-12 em-often-section-box how-often">
                                          <p>How often do you need your cleaner?</p>
                                          <ul>
                                                 <li>
                                                     <input id="how-often1" value="cash" name="how-often" class="" type="radio">
                                                     <label for="how-often1"> <span></span> &nbsp; Once</label>
                                                 </li>
                                                 
                                                 
                                                 <li>
                                                     <input id="how-often2" value="card" name="how-often" class="" checked="" type="radio">
                                                     <label for="how-often2"> <span></span> &nbsp; Weekly</label>
                                                 </li>
                                                 
                                                 
                                                 <li>
                                                     <input id="how-often3" value="card" name="how-often" class="" type="radio">
                                                     <label for="how-often3"> <span></span> &nbsp; Bi-Weekly</label>
                                                 </li>
								           </ul>
                                     </div>
                                     
                                     
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 em-date-section-box calendar-box">
                                         <p>How often do you need your cleaner?</p>
                                         <div id="toggle1" class="article">
                                              <div class="toggle-calendar1"></div>
                                         </div>
                                   </div>
                                    
                                    
                                    
                                    <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box time-set">
                                          <ul>
                                              <p>Available time <strong>15 / MARCH / 2022</strong></p>
                                              
                                              <li>
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <li>
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <li class="selected">
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <li>
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <li>
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <li>
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <li>
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <li>
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <li>
                                                  <div class="tick-mark">&nbsp;</div>
                                                  <div class="tick-text">08:00 am</div>
                                                  <div class="clear"></div>
                                              </li>
                                              
                                              <div class="clear"></div>
                                          </ul>
                                    
                                    </div>
                                    
                                    
                                    <div class="col-sm-12 em-field-main">
                                        <p>Duration in Months</p>
                                        <div class="col-12 em-text-field-main duration-list em-box-8 pl-0 pr-0">
                                             <ul class="clearfix">
                                                 <li>1</li>
                                                 <li>2</li>
                                                 <li class="selected">3</li>
                                                 <li>4</li>
                                                 <li>5</li>
                                                 <li>6</li>
                                                 <li>7</li>
                                                 <li>8</li>
                                                 <li>9</li>
                                                 <li>10</li>
                                                 <li>11</li>
                                                 <li>12</li>
                                                 
                                             </ul>
                                       </div>
                                   </div>
                                    
                                       
                                    </div>
                                    
                                    <div class="col-12 em-booking-det-cont em-next-btn">
                                       <div class="row em-next-btn-set ml-0 mr-0">
                                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                                 <span class="em-back-arrow back-to1-btn" title="Previous Step"></span>
                                                 
                                                 <div class="col-12 sp-total-price-set pl-0 pr-0">
                                                      Total<br>
                                                      <span>AED 380</span> 
                                                 </div>
                                           
                                            </div>
                                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                                 <input value="Next" class="text-field-button show3-step" type="submit">
                                            </div>
                                       </div>
                                  </div>
                               </div><!--step2 end-->
                               
                               
                               
                               
                               
                               
                               
                               
                               <div class="col-12 step3 pl-0 pr-0">
                                    <div class="col-12 em-booking-det-cont">
                                         
                                        <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box login-section">  
                                             <div class="row ml-0 mr-0">
                                                  <div class="col-md-6 login-left-set pl-0 pr-0">
                                                       
                                                       <h5>Sign In <span class="show-register-btn res-log-btn d-sm-block d-md-none"><i class="fa fa-pencil-square-o"></i> &nbsp; I am not a member yet</span></h5>
                                                       
                                                       <div class="col-sm-12 em-field-main pl-0 pr-0">
                                                            <p>Username / Email ID</p>
                                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                                 <input name="" class="text-field" type="text">
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-12 em-field-main pl-0 pr-0">
                                                            <p>Password</p>
                                                            <div class="col-12 em-text-field-main password-set pl-0 pr-0">
                                                                 <input name="" class="text-field" type="password">
                                                                 <div class="password-set-btn"><i class="fa fa-low-vision"></i></div>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-12 em-field-main pl-0 pr-0">
                                                            <p><a href="#">Did you forget your password?</a></p>
                                                       </div>
                                                  
                                                       
                                                       <div class="col-sm-12 em-field-main  log-remb pl-0 pr-0">
                                                            <input value="" id="option-01" class="" type="checkbox">
                                                            <label for="option-01"> <span></span> &nbsp; Remember Me</label>
                                                       </div>
                                                       
                                                        
                                                  </div>
                                                  
                                                  
                                                  <div class="col-md-6 login-right-set pr-0 d-none d-md-block d-xl-block">
                                                       <div class="col-sm-12 login-right-image pr-0"><img src="images/register.png" alt=""></div>
                                                       <div class="col-sm-12 login-right-btn show-register-btn pr-0">
                                                            <span><i class="fa fa-pencil-square-o"></i> &nbsp; I am not a member yet</span>
                                                       </div>
                                               </div>
                                             </div>
                                        </div><!--login-section end-->
                                        
                                        
                                        <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box register-section">  
                                             <div class="row ml-0 mr-0">
                                                  <div class="col-md-6 login-left-set pl-0 pr-0">
                                                       
                                                       <h5>Sign Up <span class="show-login-btn res-log-btn d-sm-block d-md-none"><i class="fa fa-user"></i> &nbsp; Already Signed Up</span></h5>
                                                       
                                                       <div class="col-sm-12 em-field-main pl-0 pr-0">
                                                            <p>Full Name</p>
                                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                                 <input name="" class="text-field" type="text">
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-12 em-field-main pl-0 pr-0">
                                                            <p>Phone Number</p>
                                                            <div class="col-12 em-text-field-main phone-number-set pl-0 pr-0">
                                                                 <input name="" class="text-field" type="text">
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-12 em-field-main pl-0 pr-0">
                                                            <p>Email ID</p>
                                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                                 <input name="" class="text-field" type="text">
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       <div class="col-sm-12 em-field-main pl-0 pr-0">
                                                            <p>Password</p>
                                                            <div class="col-12 em-text-field-main password-set pl-0 pr-0">
                                                                 <input name="" class="text-field" type="password">
                                                                 <div class="password-set-btn"><i class="fa fa-low-vision"></i></div>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       
                                                       
                                                  
                                                       
                                                       
                                                       
                                                        
                                                  </div>
                                                  <div class="col-md-6 login-right-set pr-0 d-none d-md-block d-xl-block">
                                                       <div class="col-sm-12 login-right-image pr-0"><img src="images/login.png" alt=""></div>
                                                       <div class="col-sm-12 login-right-btn show-login-btn pr-0">
                                                            <span><i class="fa fa-user"></i> &nbsp; Already Signed Up</span>
                                                       </div>
                                               </div>
                                             </div>
                                        </div><!--register-section end-->
                                       
                                    </div>
                                    
                                    <div class="col-12 em-booking-det-cont em-next-btn">
                                       <div class="row em-next-btn-set ml-0 mr-0">
                                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                                 <span class="em-back-arrow back-to2-btn" title="Previous Step"></span>
                                                 
                                                 <div class="col-12 sp-total-price-set pl-0 pr-0">
                                                      Total<br>
                                                      <span>AED 380</span> 
                                                 </div>
                                           
                                            </div>
                                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                                 <input value="Next" class="text-field-button show4-step" type="submit">
                                            </div>
                                       </div>
                                  </div>
                               </div><!--step3 end-->
                              
                              
                              
                              
                              
                              
                              
                              

                               <div class="col-12 step4 pl-0 pr-0">
                                    <div class="col-12 em-booking-det-cont pl-0 pr-0">
                                         <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
                                         
                                             <h5>Your Address Details</h5>
                                             
                                             <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>Area</p>
                                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                                 <select name="" type="text" placeholder="" class="el-text-field">
                                                                     <option value="">Dubai</option>
                                                                     <option>Dubai 2</option>
                                                                     <option>Dubai 3</option>
                                                                     <option>Dubai 4</option>
                                                                     <option>Dubai 5</option>
                                                                 </select>
                                                           </div>
                                                       </div>
                                                       
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>Address Details</p>
                                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                                 <input name="" class="text-field" type="text">
                                                           </div>
                                                       </div>
                                                       
                                                 </div> 
                                             </div>
                                             
                                             <!--<div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                       
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>Address</p>
                                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                                 <input name="" class="text-field" type="text">
                                                           </div>
                                                       </div>
                                                 </div> 
                                             </div>-->
                                             
                                             
                                             
                                             <div class="col-sm-12 em-field-main-set map-view">
                                                  <div class="map-search"><input name="" placeholder="Address" class="text-field" type="text"></div>
                          
                                                       <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3613.4846948105287!2d55.154268710472095!3d25.08544900023128!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f134e609581e1%3A0x5312bbe8506b0c9c!2sEmirates%20Golf%20Club-%20Majilis%20Course.!5e0!3m2!1sen!2sin!4v1590989224759!5m2!1sen!2sin" allowfullscreen="" aria-hidden="false" tabindex="0" width="100%" height="200px" frameborder="0"></iframe>
                                                 
                                             </div>
                                             

                                             
                                             <div class="col-sm-12 em-field-main pt-0">
                                             <div class="col-lg-12 col-md-12 col-sm-12 em-often-section-box how-often address-option ">
                                                  <p>Address Option</p>
                                                  <ul>
                                                         <li class="home">
                                                             <input id="address1" value="address" name="address-opt" class="" type="radio">
                                                             <label for="address1"> <span></span> &nbsp; Home</label>
                                                         </li>
                                                         
                                                         
                                                         <li class="office">
                                                             <input id="address2" value="address" name="address-opt" class="" checked="" type="radio">
                                                             <label for="address2"> <span></span> &nbsp; Office</label>
                                                         </li>
                                                         
                                                         
                                                         <li class="other">
                                                             <input id="address3" value="address" name="address-opt" class="" type="radio">
                                                             <label for="address3"> <span></span> &nbsp; Other</label>
                                                         </li>
                                                   </ul>
                                             </div>
                                     
                                             </div>
                                             
                                             
                                             
                                    
                                         </div>
                                    </div>
                                    
                                    <div class="col-12 em-booking-det-cont em-next-btn">
                                       <div class="row em-next-btn-set ml-0 mr-0">
                                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                                 <span class="em-back-arrow back-to3-btn" title="Previous Step"></span>
                                                 
                                                 <div class="col-12 sp-total-price-set pl-0 pr-0">
                                                      Total<br>
                                                      <span>AED 380</span> 
                                                 </div>
                                           
                                            </div>
                                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                                 <input value="Next" class="text-field-button show5-step" type="submit">
                                            </div>
                                       </div>
                                  </div>
                                  
                               </div><!--step4 end-->
                               
                               
                               
                               
                               
                               
                               
                               
                               
                               <div class="col-12 step5 pl-0 pr-0">
                                    <div class="col-12 em-booking-det-cont pl-0 pr-0">
                                         <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
                                         
                                             <h5>Payment Details</h5>
                                             
                                             <div class="col-sm-12 em-field-main pl-0 pr-0"> 
                                                   <div class="col-sm-12 em-extra-ser-set">
                                                     <p>Payment Mode</p>
                                                     <div class="row">
                                                     
                                                           <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                                <div class="row ml-0 mr-0 em-extra-ser-tmb selected">
                                                                     <div class="col-sm-4 em-extra-ser-img"><img src="images/em-icon21.png" alt=""></div>
                                                                     <div class="col-sm-8 em-extra-ser-cont">
                                                                          Payment via<br>
                                                                          <span>CARD</span> 
                                                                     </div>
                                                                </div>
                                                           </div>
                                                           
                                                           
                                                           <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                                                <div class="row ml-0 mr-0 em-extra-ser-tmb ">
                                                                     <div class="col-sm-4 em-extra-ser-img"><img src="images/em-icon20.png" alt=""></div>
                                                                     <div class="col-sm-8 em-extra-ser-cont">
                                                                          Payment by<br>
                                                                          <span>CASH</span> 
                                                                     </div>
                                                                </div>
                                                           </div>
                                                           
                                                           
                                                           
                                                           
                                                     </div>
                                                     
                                                </div>
                                             </div>
                                          
                                          
                                           
                                          
                                             <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                       <div class="col-sm-6 em-field-main">
                                                            <p>How to let the crew in?</p>
                                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                                 <select name="" type="text" placeholder="" class="el-text-field">
                                                                      <option value="">Select option</option>
                                                                      <option>At home</option>
                                                                      <option>Key is with security</option>
                                                                      <option>Key under the mat</option>
                                                                      <option>Buzz the intercom</option>
                                                                      <option>Door open</option>
                                                                      <option>Others</option>
                                                                 </select>
                                                           </div>
                                                       </div>
                                                 </div> 
                                             </div>
                                             
                                             
                                             
                                             
                                             <div class="col-sm-12 em-field-main-set"> 
                                                  <div class="row m-0">
                                                  
                                                       <div class="col-sm-12 em-field-main">
                                                            <p>Do you have any specific cleaning instructions?</p>
                                                            <div class="col-12 em-text-field-main pl-0 pr-0">
                                                                 <textarea name="" cols="" rows="" class="text-field-big"></textarea>
                                                           </div>
                                                       </div>
                                              
                                                 </div> 
                                             </div>
                                             
                                             
                                             
                                             
                                             
                                             <div class="col-sm-12 em-terms-and-condition-main"> 
                                             
                                                 <h6>Terms & Conditions</h6>
                                                  <div class="col-sm-12 em-terms-and-condition">
                                                        <ul class="">
                                                        
                                                            <li>
                                                                <div class="em-num">1</div>
                                                                <div class="em-num-cont"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
                                                                <div class="clear"></div>
                                                            </li>
                                                            
                                                            <li>
                                                                <div class="em-num">2</div>
                                                                <div class="em-num-cont"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p></div>
                                                                <div class="clear"></div>
                                                            </li>
                                                            
                                                            <li>
                                                                <div class="em-num">3</div>
                                                                <div class="em-num-cont"><p>When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p></div>
                                                                <div class="clear"></div>
                                                            </li>
                                                            
                                                      
                                                            <li>
                                                                <div class="em-num">5</div>
                                                                <div class="em-num-cont"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p></div>
                                                                <div class="clear"></div>
                                                            </li>
                                                            
                                                            <li>
                                                                <div class="em-num">5</div>
                                                                <div class="em-num-cont"><p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p></div>
                                                                <div class="clear"></div>
                                                            </li>
                                                            
                                                            <li>
                                                                <div class="em-num">6</div>
                                                                <div class="em-num-cont"><p>When an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</p></div>
                                                                <div class="clear"></div>
                                                            </li>
                                                            
                                                        </ul>
                                                  </div>
                                             </div>
                                             
                                             
                                             
                                             
                                             
                                             <div class="col-sm-12 em-field-main log-remb terms-check">
                                                  <input value="" id="terms" class="" type="checkbox">
                                                  <label for="terms"> <span></span> &nbsp; I accept all the terms and conditions</label>
                                             </div>
                                          
                                             
                                             
                                             
                                             
                                             
                                             
                                    
                                         </div>
                                    </div>
                                    
                                    <div class="col-12 em-booking-det-cont em-next-btn">
                                       <div class="row em-next-btn-set ml-0 mr-0">
                                            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                                 <span class="em-back-arrow back-to4-btn" title="Previous Step"></span>
                                                 
                                                 <div class="col-12 sp-total-price-set pl-0 pr-0">
                                                      Total<br>
                                                      <span>AED 380</span> 
                                                 </div>
                                           
                                            </div>
                                            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                                                 <form action="success.php">
                                                 <input value="Make Payment" class="text-field-button show6-step" type="submit">
                                                 </form>
                                            </div>
                                       </div>
                                  </div>
                                  
                               </div><!--step5 end-->
                              
                  
                  
                              
                         </div>
                         
                         
                         <div class="col-lg-4 col-md-12 col-sm-12 em-booking-content-right pl-0 pr-0">
                              <div class="col-lg-12 col-md-12 col-sm-12 em-booking-det-cont pl-0 pr-0">
                              
                                     
                                   
                                  <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0">
                                      <h5>BOOKING SUMMARY <span class="sp-sum-rem-btn"><img src="images/el-close-black.png" title=""></span></h5>
                                      
                                      <div class="col-sm-12 book-details-main service-type">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Service Type</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>House Cleaning</p></div>
                                          </div>
                                      </div>  
                                      
                                      <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Material</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>Yes</p></div>
                                          </div>
                                      </div>
                                      
                                      <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Duration</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>3 hours</p></div>
                                          </div>
                                      </div>
                                      
                                      <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Frequency</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>One-time</p></div>
                                          </div>
                                      </div>
                                      
                                      <div class="col-sm-12 book-details-main mb-3">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Number of Cleaners</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>5</p></div>
                                          </div>
                                      </div>
                                      
                                  </div>
                                  
                                  
                                  
                                  <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                       <h6>Date & Time </h6>
                                        
                                        <div class="col-sm-12 book-details-main date-time-det mb-3">
                                            <div class="row ml-0 mr-0">
                                                <div class="col-sm-12 book-det-left pl-0 pr-0"><p>Starting on Tuesday, 15th August<br>10:00 am - 01:00 pm</p></div>
                                               
                                            </div>
                                        </div>
                                        
                                  </div>
                                  
                                  
                                  
                                  
                                  
                                  
                                  <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                       <h6>Price Details</h6>
                                        
                                        <div class="col-sm-12 book-details-main">
                                            
                                            
                                            
                                            
                                            <div class="col-lg-12 col-md-12 col-sm-12 book-promo-set pl-0 pr-0">
                                                <p class="promo-text">Please enter the promo code that you've received</p>
                                                <div class="promo-main">
                                                     <input name="coupon_code" class="em-apply-field" value="" type="text">
                                                     <input name="coupon_but" class="em-apply-but" value="Check Code" type="button">
                                                </div>
                                             </div>
                                           
                                           
                                           
                                        </div>
                                        
                                        
                                        <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Price</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>AED 93.33</p></div>
                                          </div>
                                        </div>
                                      
                                      
                                        <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>VAT 5%</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>AED 4.67</p></div>
                                          </div>
                                        </div>
                                      
                                      
                                      
                                        <div class="col-sm-12 book-details-main">
                                          <div class="row total-price ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Total</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>AED 98.00</p></div>
                                          </div>
                                        </div>
                                        
                                  </div>
                                  
                                  
                                  
                                  
                                  
                              </div>
                         </div>
                         
                    </div>
               </div>






          </div>
      </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>










<?php require_once('include/footer.php') ?>



  </body>
</html>