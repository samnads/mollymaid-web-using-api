<!doctype html>
<html lang="en">
<head>
    <title>Emaid Bookig</title>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="images/favicon.png"/>
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" >
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="css/pignose.calendar.css"/>
    
</head>
<body class="bg-color success">

<header>
   <div class="container">	
      <div class="row m-0">
      
          <div class="col-lg-2 col-md-12 col-sm-12 pl-0 pr-0">
          
               <div class="em-back-arrow" title="Previous Step"></div>
               
               
               <div class="logo"><a href="index.php"><img src="images/emaid-logo.png" alt=""></a></div>
               <div class="em-mob-icons">
                    <ul>
                        <li class="mob-home"><i class="fa fa-user"></i></li>
                        <li class="mob-home"><i class="fa fa-home"></i></li>
                    </ul>
               </div>
               
               <div class="clear"></div> 
          </div>
          
          
          <div class="col-lg-10 col-sm-12  menu pl-0 pr-0">
                    <nav id="primary_nav_wrap">
                        <ul> 
                            <!--<li class="home"><a href="#">Home</a>|</li>-->
                            <li class="booking"><a href="index.php">Booking</a>|</li>
                            <li class="log-reg"><a href="#" class="pr-0">Login</a> / <a href="#" class="pl-0 pr-0">Register</a></li>
                            <div class="clear"></div>
                         </ul>
                         <div class="clear"></div>   
                     </nav>
                    <div class="clear"></div>
              
             
          </div>
      
      </div>
  </div>
</header><!--Header Section-->






<section class="em-booking-content-section">
  <div class="container em-booking-content-box">
      <div class="row em-booking-content-main ml-0 mr-0">
          <div class="col-12 em-booking-content pl-0 pr-0    min-vh-90 d-flex flex-column justify-content-center">
          
         
               
               <div class="col-12 em-booking-content-set pl-0 pr-0 mx-auto">
               	    <div class="row em-booking-content-set-main ml-0 mr-0">
                         
                        <div class="col-lg-7 col-md-12 col-sm-12 success-right pl-0 pr-0 d-sm-block d-lg-none">&nbsp;</div>
                         
                         <div class="col-lg-5 col-md-12 col-sm-12 success-left pl-0">
                              <div class="col-lg-12 col-md-12 col-sm-12 em-booking-det-cont pl-0 pr-0">
                              
                                  <div class="col-sm-12 success-message pr-0">
                                       <h3>We’ve received your booking<br><span>Please check your email</span></h3>
                                  </div>
                              
                              
                                  <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0">
                                      <h6>PERSONAL DETAILS</h6>
                                      
                                      <div class="col-sm-12 book-details-main service-type">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-5 book-det-left pl-0 pr-0"><p>Name</p></div>
                                              <div class="col-7 book-det-right pl-0 pr-0"><p>Thomas</p></div>
                                          </div>
                                      </div>  
                                      
                                      <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-5 book-det-left pl-0 pr-0"><p>Email ID</p></div>
                                              <div class="col-7 book-det-right pl-0 pr-0"><p>thomas@gmail.com</p></div>
                                          </div>
                                      </div>
                                      
                                      <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-5 book-det-left pl-0 pr-0"><p>Contact Number</p></div>
                                              <div class="col-7 book-det-right pl-0 pr-0"><p>1234567890</p></div>
                                          </div>
                                      </div>
                                      
                                      <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-5 book-det-left pl-0 pr-0"><p>Address</p></div>
                                              <div class="col-7 book-det-right pl-0 pr-0"><p>101 Dubai, Marina</p></div>
                                          </div>
                                      </div>

                                      
                                  </div>
                                  
                                  
                                  
                                  <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                    <h6>Service Details</h6>
                                        
                                        <div class="col-sm-12 book-details-main date-time-det mb-1">
                                            <div class="row ml-0 mr-0">
                                                <div class="col-sm-12 book-det-left pl-0 pr-0"><p>You've booked <strong>5 cleaners (with cleaning materials).</strong> Your <strong>one-time house cleaning service</strong> starting on <strong>Tuesday 15th August</strong> from <strong>10:00 am - 01:00 pm.</strong></p></div>
                                               
                                            </div>
                                        </div>
                                        
                                  </div>
                                  

                                  
                                  <div class="col-sm-12 book-details-main-set pl-0 pr-0">
                                    <h6>Price Details</h6>
                                        
                                        
                                        
                                        
                                        
                                    <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Price</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>AED 93.33</p></div>
                                          </div>
                                        </div>
                                      
                                      
                                        <div class="col-sm-12 book-details-main">
                                          <div class="row ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>VAT 5%</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>AED 4.67</p></div>
                                          </div>
                                        </div>
                                      
                                      
                                      
                                        <div class="col-sm-12 book-details-main">
                                          <div class="row total-price ml-0 mr-0">
                                              <div class="col-7 book-det-left pl-0 pr-0"><p>Total</p></div>
                                              <div class="col-5 book-det-right pl-0 pr-0"><p>AED 98.00</p></div>
                                          </div>
                                        </div>
                                        
                                  </div>
                                  
                                  
                              </div>
                         </div>

                         
                          <div class="col-lg-7 col-md-12 col-sm-12 success-right pr-0 d-none d-md-none d-lg-block">&nbsp;</div>
                         
                    </div>
               </div>






          </div>
      </div>
  </div>
  <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
</section>











<?php require_once('include/footer.php') ?>



  </body>
</html>