@extends('layouts.main')
@section('title', 'Failed')
@section('content')
    <section>
        <div class="container">
            <div class="row inner-wrapper m-0">
                <div class="col-sm-12 booking-success-top booking-error">
                    <div class="col-sm-12 booking-success-image"><img src="{{ asset('images/booking-failed.gif') }}"
                            alt="" /></div>
                    <div class="col-sm-12 booking-success-title">
                        <h3>Payment failed</h3>
                    </div>
                    <div class="col-sm-12 booking-success-details">
                        <ul>
                            <li>
                                <div class="col-sm-12 booking-alert-main">
                                    <div class="d-flex booking-alert">
                                        <div class="booking-alert-icon"><i class="fa fa-calendar"></i></div>
                                        <div class="booking-alert-cont flex-grow-1">
                                            <p><span>Booking Status</span><br /><strong>{{ $booking_status }}</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="col-sm-12 booking-alert-main">
                                    <div class="d-flex booking-alert">
                                        <div class="booking-alert-icon"><i class="fa fa-user-o"></i></div>
                                        <div class="booking-alert-cont flex-grow-1">
                                            <p><span>Booking Reference</span><br /><strong>{{ $reference_id }}</strong></p>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="row m-0">
                        <div class="col-lg-4 col-md-6 booking-success-left">
                            <div class="col-sm-12 book-details-main pb-2">
                                <h4>Personal Details</h4>
                            </div>

                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Name</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ $customer['customer_name'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Email ID</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ $customer['email_address'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Contact Number</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ $customer['mobile_number_1'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-4 book-det-left ps-0 pe-0">
                                        <p>Address</p>
                                    </div>
                                    <div class="col-8 book-det-right ps-0 pe-0">
                                        <p>{{ $booking_address['customer_address'] }}</p>
                                        <p>{{ $booking_address['flat_no'] }}, {{ $booking_address['building'] }}</p>
                                        <p>{{ $booking_address['area_name'] }}</p>
                                        <p>Dubai - UAE</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 booking-success-middle">
                            <div class="col-sm-12 book-details-main pb-2">
                                <h4>Service Details</h4>
                            </div>
                            @if($booking['package_name'])
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Package</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ $booking['package_name'] }}</p>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Service</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ $booking['service_type_name'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Frequency</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ $booking['frequency'] }}</p>
                                    </div>
                                </div>
                            </div>
                            @if ($booking['service_type_model_id'] == 1)
                                <div class="col-sm-12 book-details-main">
                                    <div class="row m-0">
                                        <div class="col-6 book-det-left ps-0 pe-0">
                                            <p>Duration</p>
                                        </div>
                                        <div class="col-6 book-det-right ps-0 pe-0">
                                            <p>{{ $booking['hours'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-5 book-det-left ps-0 pe-0">
                                        <p>Date & Time</p>
                                    </div>
                                    <div class="col-7 book-det-right ps-0 pe-0">
                                        <p>{{ \Carbon\Carbon::parse($booking['service_start_date'])->format('d M Y') }}</p>
                                        @if ($booking['service_type_model_id'] == 1)
                                            <p>{{ \Carbon\Carbon::parse($booking['start_time'])->format('h:i A') }} to
                                                {{ \Carbon\Carbon::parse($booking['end_time'])->format('h:i A') }}</p>
                                        @else
                                            <p>{{ \Carbon\Carbon::parse($booking['start_time'])->format('h:i A') }}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @if ($booking['service_type_model_id'] == 1 && $booking['subscription_package_id'] == null)
                                <div class="col-sm-12 book-details-main">
                                    <div class="row m-0">
                                        <div class="col-7 book-det-left ps-0 pe-0">
                                            <p>Number of Professionals</p>
                                        </div>
                                        <div class="col-5 book-det-right ps-0 pe-0">
                                            <p>{{ sizeof(@$bookings) }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if ($booking['service_type_model_id'] == 1 && $booking['subscription_package_id'] == null)
                                <div class="col-sm-12 book-details-main">
                                    <div class="row m-0">
                                        <div class="col-6 book-det-left ps-0 pe-0">
                                            <p>Material</p>
                                        </div>
                                        <div class="col-6 book-det-right ps-0 pe-0">
                                            <p>{{ ucfirst(@$booking['cleaning_materials']) }}</p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <!--<div class="col-sm-12 book-details-main">
                                        <div class="row m-0">
                                            <div class="col-5 book-det-left ps-0 pe-0">
                                                <p>Crew</p>
                                            </div>
                                            <div class="col-7 book-det-right ps-0 pe-0">
                                                @php
                                                    $maids = null;
                                                    foreach ($bookings as $key => $booking) {
                                                        $maids .= '<p>' . ($booking['maid_name'] != null ? $booking['maid_name'] : '[ Not Assigned ]') . '</p>';
                                                    }
                                                @endphp
                                                {!! $maids !!}
                                            </div>
                                        </div>
                                    </div>-->
                        </div>
                        <div class="col-lg-4 col-md-6 booking-success-middle mt-5 m-auto">
                            <div class="col-sm-12 book-details-main pb-2">
                                <h4>Price Details</h4>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Payment Method</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ $booking['payment_method'] }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Payment Status</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p>{{ $payment_status }}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row booking-amount m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Service Fee</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p><span>AED</span>
                                            {{ number_format($booking['_service_amount'], 2, '.', ',') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @if ($booking['_cleaning_materials_amount'] > 0)
                                <div class="col-sm-12 book-details-main">
                                    <div class="row booking-amount m-0">
                                        <div class="col-6 book-det-left ps-0 pe-0">
                                            <p>Materials</p>
                                        </div>
                                        <div class="col-6 book-det-right ps-0 pe-0">
                                            <p><span>AED</span>
                                                {{ number_format($booking['_cleaning_materials_amount'], 2, '.', ',') }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if ($booking['_discount_total'] > 0)
                                <div class="col-sm-12 book-details-main">
                                    <div class="row booking-amount m-0">
                                        <div class="col-6 book-det-left ps-0 pe-0">
                                            <p>Discount
                                                {{ $booking['_coupon_discount'] > 0 ? '(' . $booking['coupon_used'] . ')' : '' }}
                                            </p>
                                        </div>
                                        <div class="col-6 book-det-right ps-0 pe-0">
                                            <p><span>AED</span>
                                                {{ number_format($booking['_discount_total'], 2, '.', ',') }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-sm-12 book-details-main">
                                <div class="row booking-amount m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>Net Amount</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p><span>AED</span>
                                            {{ number_format($booking['_taxable_amount'], 2, '.', ',') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 book-details-main">
                                <div class="row booking-amount m-0">
                                    <div class="col-6 book-det-left ps-0 pe-0">
                                        <p>VAT Amount ({{ number_format($booking['_vat_percentage'], 1, '.', ',') }}%)</p>
                                    </div>
                                    <div class="col-6 book-det-right ps-0 pe-0">
                                        <p><span>AED</span>
                                            {{ number_format($booking['_vat_amount'], 2, '.', ',') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            @if ($booking['_payment_type_charge'] > 0)
                                <div class="col-sm-12 book-details-main">
                                    <div class="row booking-amount m-0">
                                        <div class="col-6 book-det-left ps-0 pe-0">
                                            <p>Convenience Fee</p>
                                        </div>
                                        <div class="col-6 book-det-right ps-0 pe-0">
                                            <p><span>AED</span>
                                                {{ number_format($booking['_payment_type_charge'], 2, '.', ',') }}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <div class="col-sm-12 book-details-main">
                                <div class="row total-price m-0">
                                    <div class="col-7 book-det-left ps-0 pe-0">
                                        <p>Total Payable</p>
                                    </div>
                                    <div class="col-5 book-det-right ps-0 pe-0">
                                        <p><span>AED</span> {{ number_format($booking['_total_payable'], 2, '.', ',') }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12">

                            <div class="col-lg-4 col-md-6 pt-4 m-auto book-details-main">
                                <div class="col-sm-12 p-1">
                                    <a href="{{ url('bookings/upcoming') }}">
                                        <input value="View Bookings" class="text-field-btn" type="submit">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush
