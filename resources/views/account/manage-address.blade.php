@extends('layouts.main')
@section('title', 'Manage Address')
@section('content')
    <section class="em-booking-content-section">
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0">
                    <div class="col-sm-12 em-step-heading-main mb-0 no-left-right-padding">
                        <div class="col-sm-12 em-step-head no-left-right-padding">
                            <h2>Address</h2>
                        </div>
                    </div>
                    <form id="default_address_form">
                        <div class="col-sm-12 my-account-main">
                            <div class="col-sm-12 em-booking-det-cont my-account-content-main">
                                <ul>
                                    <li>
                                        <div class="row my-acc-cont-main p-0 m-0">
                                            <div class="col-sm-12 my-adr-left-cont p-0">
                                                <h5>Your saved addresses <a href="{{ URL::to('add-address') }}">Add new</a>
                                                </h5>
                                            </div>
                                            <!--<div class="col-sm-6 my-adr-right-cont p-0"></div>-->
                                        </div>
                                    </li>
                                    <span id="profile-address-list-holder">
                                        <li>
                                            <div class="col-sm-12 em-field-main address-opt-main p-0">
                                                <div class="my-acc-edit"><a href=""><i class="fa fa-pencil"></i></a>
                                                </div>
                                                <div class="my-acc-delete"><a onclick="viewDeletePopup();" href="#"><i
                                                            class="fa fa-trash"></i></a></div>
                                                <input id="address-opt_1" value="{{ @$address['id'] }}" name="address_opt"
                                                    class="" title="Default Address" type="radio">
                                                <label for="address-opt_1"> <span></span> &nbsp;
                                                    <div class="col-sm-12 my-acc-cont-main address-opt  p-0">
                                                        <div class="col-sm-12 my-acc-que-cont p-0">{{ @$type }}</div>
                                                        <div class="col-sm-12 my-acc-ans-cont p-0">
                                                            <input name="" class="text-field" type="text"
                                                                value="{{ @$address['addressName'] }},{{ @$address['Area'] }}"
                                                                disabled>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        </li>
                                    </span>
                                </ul>
                            </div>
                            <div class="col-12 em-booking-det-cont em-next-btn">
                                <div class="row em-next-btn-set ml-0 mr-0">
                                    <div class="col-md-6 col-sm-12 col-6 em-next-btn-left pl-0 pr-0">
                                        <a class="cursor" href="{{ URL::to('profile') }}"> <span
                                                class="em-back-arrow back-to4-btn " title="My Account"></span></a>
                                    </div>
                                    <div class="col-sm-6 col-12 em-next-btn-right pl-0 pr-0">
                                        <button class="text-field-button show6-step"
                                            type="submit">Set Default Address</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
    </section>
@endsection
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.toast.css?v=1.0') }}">
@endpush
@push('scripts')
<script type="text/javascript" src="{{ asset('js/jquery.toast.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/profile.js?v=') . Config::get('version.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-locationpicker/0.1.12/locationpicker.jquery.min.js"
        integrity="sha512-KGE6gRUEc5VBc9weo5zMSOAvKAuSAfXN0I/djLFKgomlIUjDCz3b7Q+QDGDUhicHVLaGPX/zwHfDaVXS9Dt4YA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
        $(document).ready(function() {
            fetchAddressList();
        });

        function showPosition(position) {
            $('#latitude').val(position.coords.latitude);
            $('#longitude').val(position.coords.longitude);
            locationPickr(position.coords.latitude, position.coords.longitude);
        }

        function showError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    $('.us3').locationpicker({
                        location: {
                            latitude: 25.055277,
                            longitude: 55.1586003
                        },
                        radius: 0,
                        inputBinding: {
                            latitudeInput: $('#latitude'),
                            longitudeInput: $('#longitude'),
                            radiusInput: $('.us3-radius'),
                            locationNameInput: $('.us3-address')
                        },
                        enableAutocomplete: true,
                        onchanged: function(currentLocation, radius, isMarkerDropped) {
                            // Uncomment line below to show alert on each Location Changed event
                            //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                        }
                    });
                    break;
                case error.POSITION_UNAVAILABLE:
                    console.log("Location information is unavailable.");
                    break;
                case error.TIMEOUT:
                    console.log("The request to get user location timed out.");
                    break;
                case error.UNKNOWN_ERROR:
                    console.log("An unknown error occurred.");
                    break;
            }
        }

        function locationPickr(latitude, longitude) {
            $('.us3').locationpicker({
                location: {
                    latitude: latitude,
                    longitude: longitude
                },

                radius: 0,
                inputBinding: {
                    latitudeInput: $('#latitude'),
                    longitudeInput: $('#longitude'),
                    radiusInput: $('.us3-radius'),
                    locationNameInput: $('.us3-address')
                },
                //markerIcon: _base_url +'images/picker.png',
                enableAutocomplete: true,
                onchanged: function(currentLocation, radius, isMarkerDropped) {
                    // Uncomment line below to show alert on each Location Changed event
                    //alert("Location changed. New location (" + currentLocation.latitude + ", " + currentLocation.longitude + ")");
                }
            });
        }
        $(document).ready(function() {
            if ($('#latitude').val() == '' || $('#longitude').val() == '') {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(showPosition, showError);
                } else {
                    x.innerHTML = "Geolocation is not supported by this browser.";
                }
            } else {
                locationPickr($('#latitude').val(), $('#longitude').val());
            }
        });
    </script>
@endpush
