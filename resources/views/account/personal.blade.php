@extends('layouts.main')
@section('title', 'My Account')
@section('content')
    <section class="em-booking-content-section">
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0">
                    <div class="col-sm-12 em-step-heading-main mb-0 no-left-right-padding">
                        <div class="col-sm-12 em-step-head no-left-right-padding">
                            <h2>Personal Details</h2>
                        </div>
                    </div>
                    <div class="col-sm-12 my-account-main">
                        <form id="profile_update" novalidate="novalidate">
                            <div class="col-sm-12 em-booking-det-cont my-account-content-main">
                                <ul>
                                    <li class="edit-details">
                                        <div class="col-sm-12 my-acc-cont-main p-0">
                                            <div class="col-sm-12 my-acc-que-cont p-0">Full Name</div>
                                            <div class="col-sm-12 my-acc-ans-cont p-0">
                                                <input name="name" class="text-field" type="text"
                                                    value="{{ session('customer_name') }}">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-sm-12 my-acc-cont-main p-0">
                                            <div class="col-sm-12 my-acc-que-cont p-0">Mobile Number</div>
                                            <div class="col-sm-12 my-acc-ans-cont p-0">
                                                <input name="mobilenumber" class="text-field" type="tel"
                                                    value="{{ session('customer_mobile') }}">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="col-sm-12 my-acc-cont-main p-0">
                                            <div class="col-sm-12 my-acc-que-cont p-0">Email</div>
                                            <div class="col-sm-12 my-acc-ans-cont p-0">
                                                <input name="email" class="text-field" type="text"
                                                    value="{{ session('customer_email') }}">
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 em-booking-det-cont em-next-btn">
                                <div class="row em-next-btn-set ml-0 mr-0">
                                    <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                                        <a class="cursor" href="{{ URL::to('profile') }}"><span
                                                class="em-back-arrow back-to1-btn" title="Home"></span></a>
                                    </div>
                                    <div class="col-sm-6 col-12 em-next-btn-right pl-0 pr-0">
                                        <button class="text-field-button show6-step" type="submit">Update</button </div>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
    </section>
@endsection
@push('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.toast.css?v=1.0') }}">
@endpush
@push('scripts')
<script type="text/javascript" src="{{ asset('js/jquery.toast.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.edit-profile-name-btn').click(function() {
                $("#profileName").attr("readonly", false);
            });
            $('.edit-profile-mobile-btn').click(function() {
                $("#profileMobile").attr("readonly", false);
            });
            $('.edit-profile-email-btn').click(function() {
                $("#profileEmail").attr("readonly", false);
            });

            login_form_validator = $('#profile_update').validate({
                focusInvalid: false,
                ignore: [],
                rules: {
                    "name": {
                        required: true,
                    },
                    "mobilenumber": {
                        required: true,
                        minlength: 9,
                        maxlength: 9
                    },
                    "email": {
                        required: true,
                    }
                },
                messages: {
                    "name": {
                        required: "Select your name",
                    },
                    "mobilenumber": {
                        required: "Enter mobile number",
                    },
                    "email": {
                        required: "Select your name",
                    }
                },
                errorPlacement: function(error, element) {
                    error.insertAfter(element);
                },
                submitHandler: function(form) {
                    let submit_btn = $('button[type="submit"]', form);
                    submit_btn.html(loading_button_html).prop("disabled", true);
                    $.ajax({
                        type: 'POST',
                        url: _base_url + "api/customer/update_customer_data",
                        dataType: 'json',
                        data: $('#profile_update').serialize(),
                        success: function(response) {
                            submit_btn.html('Update').prop("disabled", false);
                            if (response.result.status == "success") {
                                if (response.result.check_otp == true) {
                                    // customer trying to change mobile
                                    // we have already send otp
                                    toast('OTP Sent', response.result.message,
                                        'info');
                                    $('#login-otp-popup-form input[name="mobilenumber"]')
                                        .val(response.result.UserDetails.current_mobile);
                                    $('#login-otp-popup-form input[name="new_mobilenumber"]')
                                        .val(response.result.UserDetails.mobile);
                                    //
                                    //let oldnum = $("#oldmobilenumber").val();
                                    // $('.login-popup').hide(500);
                                    //$('#login-otp-popup-form input[name="mobilenumber"]').val(oldnum);
                                    //$('#login-otp-popup-form input[name="oldmobilenumber"]').val(response.result.UserDetails.mobile);
                                    //$('#login-otp-popup-form input[name="is_update"]').val(1);
                                    $('.customer-full-mobile').html(response.result
                                        .UserDetails.mobile)
                                    showOtp(true);
                                } else {
                                    toast('Success', response.result.message, 'success');
                                }
                            } else {

                            }
                        },
                        error: function(response) {
                            submit_btn.html('Update').prop("disabled", false);
                        },
                    });
                }
            });
        });
    </script>
@endpush
