@extends('layouts.main', [])
@section('title', 'Profile')
@section('content')
    <section class="em-booking-content-section">
        <div class="container em-booking-content-box">
            <div class="row em-booking-content-main ml-0 mr-0">
                <div class="col-12 em-booking-content pl-0 pr-0">
                    <div class="col-sm-12 em-step-heading-main mb-0 no-left-right-padding">
                        <div class="col-sm-12 em-step-head no-left-right-padding">
                            <h2>My Account</h2>
                            @if (session('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 my-account-main">
                        <div class="col-sm-12 my-account-title-main mb-4">
                            <div class="my-account-title-photo-main">
                                <div class="my-account-title-photo">&nbsp;</div>
                            </div>
                            <div class="col-sm-12 my-account-title-cont">
                                <h5 class="text-center">
                                    @if (session('fullName'))
                                        {{ session('fullName') }}
                                    @endif
                                </h5>
                                <p class="text-center">
                                    @if (session('phone'))
                                        {{ session('phone') }}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="col-sm-12 em-booking-det-cont my-account-content-main">
                            <ul class="link-set">
                                <li><a href="{{ url('profile/personal-details') }}">Edit Personal Details</a></li>
                                <li><a href="{{ url('profile/manage-address') }}">Manage Address</a></li>
                                <li><a onclick="viewChangePasswordPopup(<?php echo session('customerId'); ?>);" href="#">Change
                                        Password</a></li>
                                <!-- <li><a href="{{ url('manage-cards/' . session('customerId')) }}">Manage Cards</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.12/cropper.min.css">
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.12/cropper.min.js"></script>
    <script type="text/javascript" defer>
        var avatar_crop_form = $('#avatar-crop-popup-form');
        var cropper;
        $('[data-action="close"]', avatar_crop_form).click(function() {
            closeCropper();
        });
        function showCropper(image, cropWidth, cropHeight) {
            console.log('showCropper called');
            cropper = new Cropper(image, {
                aspectRatio: cropWidth / cropHeight,
                viewMode: 1,
            });
            $('#avatar-crop-popup').show(500);
        }
        window.addEventListener('DOMContentLoaded', function() {
            var image = document.getElementById('image');
            var input = document.querySelector('label[data-action="select-file"] input[type="file"]');
            var cropButton = document.getElementById('crop');
            input.addEventListener('change', function(e) {
                var files = e.target.files;
                var done = function(url) {
                    input.value = '';
                    image.src = url;
                    image.onload = function() {
                        showCropper(image, 500, 500);
                    };
                };
                var reader;
                var file;
                if (files && files.length > 0) {
                    file = files[0];
                    if (URL) {
                        done(URL.createObjectURL(file));
                    } else if (FileReader) {
                        reader = new FileReader();
                        reader.onload = function(e) {
                            done(reader.result);
                        };
                        reader.readAsDataURL(file);
                    }
                }
            });
            if (cropButton) {
                cropButton.addEventListener('click', function() {
                    var canvas;
                    if (cropper) {
                        canvas = cropper.getCroppedCanvas({
                            width: 500,
                            height: 500,
                        });
                        var src = canvas.toDataURL();
                        $('input[name="image"]', avatar_crop_form).val(src);
                        let update_btn = $('button[name="update-avatar"]', avatar_crop_form);
                        update_btn.html(loading_button_html).prop("disabled", true);
                        $.ajax({
                            url: _base_url + 'api/customer/update_avatar',
                            type: 'POST',
                            data: $('#avatar-crop-popup-form').serialize(),
                            success: function(response) {
                                update_btn.html('Update').prop("disabled", false);
                                if (response.result && response.result.status === 'success') {
                                    var userPhoto = document.querySelector('.user-photo img');
                                    userPhoto.src = src; // not using link because of UX
                                    toast('Updated', response.result.message, 'success');
                                } else {
                                    toast('Failed', response.result.message, 'error');
                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                update_btn.html('Update').prop("disabled", false);
                            }
                        });
                    }
                    closeCropper();
                });
            }
        });
        function closeCropper() {
            if (cropper) {
                cropper.destroy();
                cropper = null;
            }
            $('#avatar-crop-popup').hide(500);
        }
    </script>
@endpush
