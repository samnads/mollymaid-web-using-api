<footer>
  <div class="container">
      <div class="row footer-main m-0">
          <div class="col-lg-12 col-md-12 col-sm-12 footer-copy-right pl-0 pr-0">
              <div class="row ml-0 mr-0">
                  <div class="col-6 footer-left p-0"><p class="copy-right">Emaid © 2022 All Rights Reserved.</p></div>
                  <div class="col-6 footer-right p-0">
                       <div class="design">
                            <a href="http://azinovatechnologies.com/" target="_blank">
                               <div class="azinova-logo">&nbsp;</div>
                            </a>
                            <p class="no-padding">Powered by </p>
                            <div class="clear"></div>
                       </div>
                  </div>
              </div>
          </div>          
      </div>
  </div>
</footer>