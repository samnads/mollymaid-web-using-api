<script type="text/javascript">
    var _base_url = "{{ url('') }}/";
    var _api_url = "{{ Config::get('url.api_url') }}";
    var _current_url = "{{ strtok(url()->full(), '?') }}";
    let _coupon_code = null;
    let _checkout_primary_key = null;
    let _google_pay_environment = "TEST";
    /***** user based data **************/
    var _id = {{ @session('customer_id') ? @session('customer_id') : 'null' }}; // customer id
    var _name = {!! @session('customer_name') ? '"' . session('customer_name') . '"' : 'null' !!}; // customer name
    var _email = {!! @session('customer_email') ? '"' . session('customer_email') . '"' : 'null' !!}; // customer email
</script>
<script type="text/javascript" src="{{ asset('js/jquery-3.7.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/js.cookie.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.toast.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.query-object.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" src="{{ asset('js/main.js?v=') . Config::get('version.js') }}"></script>
<!--<script type="text/javascript" src="{{ asset('js/login.js?v=') . Config::get('version.js') }}"></script>-->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB0ztqRwv8zz7IMF7NTCcfF2MIw488DKR4&libraries=places&callback=google_maps_callback"></script>
