<header style="display: {{isWebView() ? 'none':'block'}}">
    <div class="container">
        <div class="row m-0">
            <div class="col-lg-2 col-md-12 col-sm-12 pl-0 pr-0">
                <div class="em-back-arrow" title="Previous Step" data-action="prev-step-go"></div>
                <div class="logo"><a href="{{url('')}}"><img src="images/emaid-logo.png" alt=""></a></div>
                <div class="em-mob-icons">
                    <ul>
                        <li class="mob-home"><a href="{{url('')}}"><i class="fa fa-user"></i></a></li>
                        <li class="mob-home"><a href="{{url('')}}"><i class="fa fa-home"></i></a></li>
                    </ul>
                </div>
                <div class="clear"></div>
            </div>
            <div class="col-lg-10 col-sm-12  menu pl-0 pr-0">
                <nav id="primary_nav_wrap">
                    <ul class="before-login" style="display: {{session('customer_id') ? 'none' : 'block'}}">
                        <li class="booking"><a href="{{url('')}}">Home</a>|</li>
                        <li class="log-reg" data-action="login-popup"><a href="#" class="pr-0">Login</a></li>
                        <div class="clear"></div>
                    </ul>
                    <ul class="after-login" style="display: {{session('customer_id') ? 'block' : 'none'}}">
                        <li class="booking"><a href="{{url('')}}">Home</a>|</li>
                        <li class="log-reg" data-action="logout"><a href="#" class="pr-0">Logout</a></li>
                        <div class="clear"></div>
                    </ul>
                    <div class="clear"></div>
                </nav>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</header><!--Header Section-->