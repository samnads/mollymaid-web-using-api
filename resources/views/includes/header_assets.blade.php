<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/pignose.calendar.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/style1.css?v=' . Config::get('version.css')) }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/css_change1.css?v=' . Config::get('version.css')) }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css?v=' . Config::get('version.css')) }}" />