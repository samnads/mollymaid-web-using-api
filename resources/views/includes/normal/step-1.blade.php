<div class="col-12 step1 pl-0 pr-0 step-1">
    <div class="col-12 em-booking-det-cont book-det-cont-set pl-0 pr-0 mt-0">
        <div class="col-12 book-det-cont-set-main house-cleaning-service ser-cont1 ser-cont" id="cont1">
            <h5>House Cleaning Services</h5>
            <div class="col-sm-12 em-field-main-set">
                <div class="row m-0">
                    <div class="col-sm-6 em-field-main">
                        <p>No. of Hours</p>
                        <div class="col-12 pl-0 radio-checkbox-boxed">
                            <ul>
                                @for ($i = 2; $i <= 7; $i++)
                                    <li>
                                        <input id="hours_{{ $i }}" value="{{ $i }}" name="hours"
                                            type="radio">
                                        <label for="hours_{{ $i }}">{{ $i }}</label>
                                    </li>
                                @endfor
                            </ul>
                        </div>
                    </div>
                    <input name="hours" type="hidden" value="2"/>
                    <div class="col-sm-6 em-field-main">
                        <p>No. of maids</p>
                        <div class="col-12 em-text-field-main how-many-maids pl-0 pr-0">
                            <ul class="clearfix">
                                <li class="em-minus" data-action="maid-minus">&nbsp;</li>
                                <li class="num-maids">
                                    <input name="professionals_count" value="1" class="text-field" type="text" readonly>
                                </li>
                                <li class="em-plus" data-action="maid-plus">&nbsp;</li>
                            </ul>
                        </div>
                    </div>
                    <input name="professionals_count" type="hidden" value="1"/>
                </div>
            </div>
            <div class="col-sm-12 em-field-main-set">
                <div class="row m-0">
                    <div class="col-sm-6 em-field-main">
                        <p>Do you want cleaning materials ?</p>
                        <div class="col-12 pl-0 pr-0 radio-checkbox-boxed">
                            <ul class="checkbox-tabs">
                                <li>
                                    <input id="material_1" value="1" name="cleaning_materials" type="radio">
                                    <label for="material_1">Yes</label>
                                </li>
                                <li>
                                    <input id="material_0" value="0" name="cleaning_materials" type="radio">
                                    <label for="material_0">No</label>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 em-terms-and-condition-main">

                <h6>What to expect from the service?</h6>
                <div class="col-sm-12 em-terms-and-condition">
                    <ul class="">

                        <li>
                            <div class="em-num">1</div>
                            <div class="em-num-cont">
                                <p>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry.</p>
                            </div>
                            <div class="clear"></div>
                        </li>

                        <li>
                            <div class="em-num">2</div>
                            <div class="em-num-cont">
                                <p>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the
                                    industry's standard dummy text ever since the 1500s.</p>
                            </div>
                            <div class="clear"></div>
                        </li>

                        <li>
                            <div class="em-num">3</div>
                            <div class="em-num-cont">
                                <p>When an unknown printer took a galley of type and
                                    scrambled it to make a type specimen book. It has
                                    survived not only five centuries,</p>
                            </div>
                            <div class="clear"></div>
                        </li>


                        <li>
                            <div class="em-num">5</div>
                            <div class="em-num-cont">
                                <p>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry.</p>
                            </div>
                            <div class="clear"></div>
                        </li>

                        <li>
                            <div class="em-num">5</div>
                            <div class="em-num-cont">
                                <p>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the
                                    industry's standard dummy text ever since the 1500s.</p>
                            </div>
                            <div class="clear"></div>
                        </li>

                        <li>
                            <div class="em-num">6</div>
                            <div class="em-num-cont">
                                <p>When an unknown printer took a galley of type and
                                    scrambled it to make a type specimen book. It has
                                    survived not only five centuries,</p>
                            </div>
                            <div class="clear"></div>
                        </li>

                    </ul>
                </div>
            </div>
            <input name="cleaning_materials" type="hidden" value="1"/>
        </div><!--house-cleaning-service end-->
    </div>
    <div class="col-12 em-booking-det-cont em-next-btn">
        <div class="row em-next-btn-set ml-0 mr-0">
            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                <div class="col-12 sp-total-price-set pl-0 pr-0">
                    Total<br>
                    <span>AED <calc-amount class="total_payable">00.00</calc-amount></span>
                </div>
            </div>
            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                <button class="text-field-button show2-step" type="button" data-action="next-step" data-step="1">Next</button>
            </div>
        </div>
    </div>
</div><!--step1 end-->