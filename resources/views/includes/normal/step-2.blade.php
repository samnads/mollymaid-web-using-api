<div class="col-12 step2 pl-0 pr-0 step-2">
    <div class="col-12 em-booking-det-cont book-det-cont-set ">

        <div class="col-lg-12 col-md-12 col-sm-12 em-often-section-box how-often">
            <p>How often do you need your cleaner?</p>
            <ul>
                @foreach($api_data['frequency_list'] as $key => $frequency)
                <li>
                    <input id="frequency_{{$key}}" value="{{$frequency['code']}}" name="frequency" type="radio" data-name="{{$frequency['name']}}">
                    <label for="frequency_{{$key}}"> <span></span>{{$frequency['name']}}</label>
                </li>
                @endforeach
            </ul>
        </div>



        <div class="col-lg-12 col-md-12 col-sm-12 em-date-section-box calendar-box">
            <p>How often do you need your cleaner?</p>
            <div id="toggle1" class="article">
                <div class="toggle-calendar1"></div>
            </div>
        </div>
        <input name="date" type="hidden"/>



        <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box time-set">
            <p>Available time <strong>15 / MARCH / 2022</strong></p>
            <ul id="times-holder">
            </ul>
        </div>



    </div>

    <div class="col-12 em-booking-det-cont em-next-btn">
        <div class="row em-next-btn-set ml-0 mr-0">
            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                <span class="em-back-arrow back-to1-btn" title="Previous Step" data-action="prev-step" data-step="2"></span>

                <div class="col-12 sp-total-price-set pl-0 pr-0">
                    Total<br>
                    <span>AED <calc-amount class="total_payable">00.00</calc-amount></span>
                </div>

            </div>
            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                <button class="text-field-button show3-step" type="button" data-action="next-step" data-step="2">Next</button>
            </div>
        </div>
    </div>
</div><!--step2 end-->
