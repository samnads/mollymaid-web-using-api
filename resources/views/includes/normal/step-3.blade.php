<div class="col-12 step3 pl-0 pr-0 step-3">
    <div class="col-12 em-booking-det-cont">
        <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box login-section">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 login-left-set pl-0 pr-0">
                    <h5>Sign In <span class="show-register-btn res-log-btn d-sm-block d-md-none"><i
                                class="fa fa-pencil-square-o"></i> &nbsp; I am not a member
                            yet</span></h5>

                    <div class="col-sm-12 em-field-main pl-0 pr-0">
                        <p>Phone Number</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <input name="email" class="text-field" type="text">
                        </div>
                    </div>
                    <div class="col-sm-12 em-field-main pl-0 pr-0">
                        <p>Password</p>
                        <div class="col-12 em-text-field-main password-set pl-0 pr-0">
                            <input name="password" class="text-field" type="password">
                            <div class="password-set-btn"><i class="fa fa-low-vision"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 em-field-main pl-0 pr-0">
                        <p><a href="#">Did you forget your password?</a></p>
                    </div>
                    <div class="col-sm-12 em-field-main  log-remb pl-0 pr-0">
                        <input value="" id="option-01" class="" type="checkbox">
                        <label for="option-01"> <span></span> &nbsp; Remember Me</label>
                    </div>
                </div>
                <div class="col-md-6 login-right-set pr-0 d-none d-md-block d-xl-block">
                    <div class="col-sm-12 login-right-image pr-0"><img src="images/register.png" alt=""></div>
                    <div class="col-sm-12 login-right-btn show-register-btn pr-0">
                        <span><i class="fa fa-pencil-square-o"></i> &nbsp; Not having account ?</span>
                    </div>
                </div>
            </div>
        </div><!--login-section end-->
        <div class="col-lg-12 col-md-12 col-sm-12 em-time-section-box register-section">
            <div class="row ml-0 mr-0">
                <div class="col-md-6 login-left-set pl-0 pr-0">
                    <h5>Sign Up <span class="show-login-btn res-log-btn d-sm-block d-md-none"><i class="fa fa-user"></i>
                            &nbsp; Already Signed Up ?</span>
                    </h5>
                    <div class="col-sm-12 em-field-main pl-0 pr-0">
                        <p>Full Name</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <input name="signup_name" class="text-field" type="text" value="Samnad S">
                        </div>
                    </div>
                    <div class="col-sm-12 em-field-main pl-0 pr-0">
                        <p>Phone Number</p>
                        <div class="col-12 em-text-field-main phone-number-set pl-0 pr-0">
                            <input name="signup_mobile" class="text-field" type="text" style="padding-left: 50px;"
                                value="979797979">
                        </div>
                    </div>
                    <div class="col-sm-12 em-field-main pl-0 pr-0">
                        <p>Email ID</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <input name="signup_email" class="text-field" type="text" value="samnad.s@azinova.info">
                        </div>
                    </div>
                    <div class="col-sm-12 em-field-main pl-0 pr-0">
                        <p>Password</p>
                        <div class="col-12 em-text-field-main password-set pl-0 pr-0">
                            <input name="signup_password" class="text-field" type="password" value="password">
                            <div class="password-set-btn"><i class="fa fa-low-vision"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 login-right-set pr-0 d-none d-md-block d-xl-block">
                    <div class="col-sm-12 login-right-image pr-0"><img src="images/login.png" alt=""></div>
                    <div class="col-sm-12 login-right-btn show-login-btn pr-0">
                        <span><i class="fa fa-user"></i> &nbsp; I've an account</span>
                    </div>
                </div>
            </div>
        </div><!--register-section end-->
    </div>
    <div class="col-12 em-booking-det-cont em-next-btn">
        <div class="row em-next-btn-set ml-0 mr-0">
            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                <span class="em-back-arrow back-to2-btn" title="Previous Step" data-action="prev-step"
                    data-step="3"></span>

                <div class="col-12 sp-total-price-set pl-0 pr-0">
                    Total<br>
                    <span>AED <calc-amount class="total_payable">00.00</calc-amount></span>
                </div>
            </div>
            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                <button class="text-field-button" type="button" data-action="step-signup">Signup</button>
                <button class="text-field-button" type="button" data-action="step-login" style="display: none">Login</button>
            </div>
        </div>
    </div>
</div><!--step3 end-->
