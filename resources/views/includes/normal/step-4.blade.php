<div class="col-12 step4 pl-0 pr-0 step-4">
    <div class="col-12 em-booking-det-cont pl-0 pr-0">
        <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0 address-section">
            <h5>Your Address Details</h5>
            <div class="col-sm-12 em-field-main-set">
                <div class="row m-0">
                    <div class="col-sm-6 em-field-main">
                        <p>Area</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <select name="area_id" type="text" placeholder="" class="el-text-field">
                                <option value="">-- Select Area --</option>
                                @foreach ($api_area_list['data'] as $key => $area)
                                    <option value="{{ $area['id'] }}">{{ $area['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 em-field-main">
                        <p>Unit / Room No.</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <input name="unit_no" class="text-field" type="text">
                        </div>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-sm-6 em-field-main">
                        <p>Building</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <input name="building" class="text-field" type="text">
                        </div>
                    </div>
                </div>
            </div>

            <!--<div class="col-sm-12 em-field-main-set">
                <div class="row m-0">

                    <div class="col-sm-6 em-field-main">
                        <p>Address</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <input name="" class="text-field" type="text">
                        </div>
                    </div>
                </div>
            </div>-->



            <div class="col-sm-12 em-field-main-set map-view">
                <div class="map-search"><input name="" placeholder="Address" class="text-field" type="text">
                </div>

                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3613.4846948105287!2d55.154268710472095!3d25.08544900023128!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f134e609581e1%3A0x5312bbe8506b0c9c!2sEmirates%20Golf%20Club-%20Majilis%20Course.!5e0!3m2!1sen!2sin!4v1590989224759!5m2!1sen!2sin"
                    allowfullscreen="" aria-hidden="false" tabindex="0" width="100%" height="200px"
                    frameborder="0"></iframe>

            </div>



            <div class="col-sm-12 em-field-main pt-0">
                <div class="col-lg-12 col-md-12 col-sm-12 em-often-section-box how-often address-option ">
                    <p>Address Option</p>
                    <ul>
                        @foreach ($api_data['residence_types'] as $key => $residence_type)
                        <li class="{{strtolower($residence_type['name'])}} pb-2">
                            <input id="residence_type_{{$key}}" value="{{$residence_type['id']}}" name="residence_type" class="" type="radio" {{ ($key == 0) ? 'checked' : '' }}>
                            <label for="residence_type_{{$key}}"><span></span> {{$residence_type['name']}}</label>
                        </li>
                        @endforeach
                    </ul>
                </div>

            </div>




        </div>
    </div>

    <div class="col-12 em-booking-det-cont em-next-btn">
        <div class="row em-next-btn-set ml-0 mr-0">
            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                <span class="em-back-arrow back-to3-btn" title="Previous Step" data-action="prev-step"
                    data-step="4"></span>

                <div class="col-12 sp-total-price-set pl-0 pr-0">
                    Total<br>
                    <span>AED <calc-amount class="total_payable">00.00</calc-amount></span>
                </div>

            </div>
            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                <button class="text-field-button show5-step" type="button" data-action="step-save-address">Next</button>
            </div>
        </div>
    </div>

</div><!--step4 end-->
