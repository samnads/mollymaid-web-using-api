<div class="col-12 step5 pl-0 pr-0 step-5">
    <div class="col-12 em-booking-det-cont pl-0 pr-0">
        <div class="col-sm-12 book-det-cont-set-main pl-0 pr-0">
            <h5>Payment Details</h5>
            <div class="col-lg-12 col-md-12 col-sm-12 ml-3 em-often-section-box how-often">
                <p>How often do you need your cleaner?</p>
                <ul>
                    @foreach ($api_data['payment_types'] as $key => $payment_type)
                        @if ($payment_type['show_in_web'] == 1)
                            <li>
                                <input id="payment-method-{{ $key }}" value="{{ $payment_type['id'] }}"
                                    name="payment_method" type="radio" data-name="{{ $payment_type['name'] }}">
                                <label for="payment-method-{{ $key }}">
                                    <span></span>{{ $payment_type['name'] }}</label>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-12 em-field-main-set">
                <div class="row m-0">
                    <div class="col-sm-6 em-field-main">
                        <p>How to let the crew in?</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <select name="" type="text" placeholder="" class="el-text-field">
                                <option value="">Select option</option>
                                <option>At home</option>
                                <option>Key is with security</option>
                                <option>Key under the mat</option>
                                <option>Buzz the intercom</option>
                                <option>Door open</option>
                                <option>Others</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 em-field-main-set">
                <div class="row m-0">

                    <div class="col-sm-12 em-field-main">
                        <p>Do you have any specific cleaning instructions?</p>
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <textarea name="" cols="" rows="" class="text-field-big"></textarea>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-12 em-terms-and-condition-main">
                <h6>Terms & Conditions</h6>
                <div class="col-sm-12 em-terms-and-condition">
                    <ul class="">
                        <li>
                            <div class="em-num">1</div>
                            <div class="em-num-cont">
                                <p>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry.</p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <div class="em-num">2</div>
                            <div class="em-num-cont">
                                <p>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the
                                    industry's standard dummy text ever since the 1500s.</p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <div class="em-num">3</div>
                            <div class="em-num-cont">
                                <p>When an unknown printer took a galley of type and
                                    scrambled it to make a type specimen book. It has
                                    survived not only five centuries,</p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <div class="em-num">5</div>
                            <div class="em-num-cont">
                                <p>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry.</p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <div class="em-num">5</div>
                            <div class="em-num-cont">
                                <p>Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the
                                    industry's standard dummy text ever since the 1500s.</p>
                            </div>
                            <div class="clear"></div>
                        </li>
                        <li>
                            <div class="em-num">6</div>
                            <div class="em-num-cont">
                                <p>When an unknown printer took a galley of type and
                                    scrambled it to make a type specimen book. It has
                                    survived not only five centuries,</p>
                            </div>
                            <div class="clear"></div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-12 em-field-main log-remb terms-check">
                <input value="" id="terms" class="" type="checkbox">
                <label for="terms"> <span></span> &nbsp; I accept all the terms and
                    conditions</label>
            </div>
        </div>
    </div>
    <div class="col-12 em-booking-det-cont em-next-btn">
        <div class="row em-next-btn-set ml-0 mr-0">
            <div class="col-sm-6 col-6 em-next-btn-left pl-0 pr-0">
                <span class="em-back-arrow back-to4-btn" title="Previous Step" data-action="prev-step"
                    data-step="5"></span>

                <div class="col-12 sp-total-price-set pl-0 pr-0">
                    Total<br>
                    <span>AED 380</span>
                </div>

            </div>
            <div class="col-sm-6  col-6 em-next-btn-right pl-0 pr-0">
                <button class="text-field-button show6-step" type="submit">Make Payment</button>
            </div>
        </div>
    </div>
</div><!--step5 end-->
