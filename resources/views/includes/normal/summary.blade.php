<div class="col-lg-12 col-md-12 col-sm-12 em-booking-det-cont pl-0 pr-0" id="booking-summary">



    <div class="col-sm-12 book-details-main-set summary-set clearfix pl-0 pr-0">
        <h5>BOOKING SUMMARY <span class="sp-sum-rem-btn"><img src="images/el-close-black.png" title=""></span></h5>

        <div class="col-sm-12 book-details-main service-type">
            <div class="row ml-0 mr-0">
                <div class="col-7 book-det-left pl-0 pr-0">
                    <p>Service Type</p>
                </div>
                <div class="col-5 book-det-right pl-0 pr-0">
                    <p>House Cleaning</p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 book-details-main">
            <div class="row ml-0 mr-0">
                <div class="col-7 book-det-left pl-0 pr-0">
                    <p>Material</p>
                </div>
                <div class="col-5 book-det-right pl-0 pr-0">
                    <p class="is_materials_included">-</p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 book-details-main">
            <div class="row ml-0 mr-0">
                <div class="col-7 book-det-left pl-0 pr-0">
                    <p>Duration</p>
                </div>
                <div class="col-5 book-det-right pl-0 pr-0">
                    <p class="duration">-</p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 book-details-main">
            <div class="row ml-0 mr-0">
                <div class="col-7 book-det-left pl-0 pr-0">
                    <p>Frequency</p>
                </div>
                <div class="col-5 book-det-right pl-0 pr-0">
                    <p class="frequency_name">-</p>
                </div>
            </div>
        </div>

        <div class="col-sm-12 book-details-main mb-3">
            <div class="row ml-0 mr-0">
                <div class="col-7 book-det-left pl-0 pr-0">
                    <p>Number of Professionals</p>
                </div>
                <div class="col-5 book-det-right pl-0 pr-0">
                    <p class="professionals_count">-</p>
                </div>
            </div>
        </div>

    </div>



    <div class="col-sm-12 book-details-main-set pl-0 pr-0">
        <h6>Date & Time </h6>

        <div class="col-sm-12 book-details-main date-time-det mb-3 step-2-show">
            <div class="row ml-0 mr-0">
                <div class="col-sm-12 book-det-left pl-0 pr-0">
                    <p class="date"></p>
                </div>

            </div>
        </div>

    </div>






    <div class="col-sm-12 book-details-main-set pl-0 pr-0">
        <h6>Price Details</h6>

        <div class="col-sm-12 book-details-main">




            <div class="col-lg-12 col-md-12 col-sm-12 book-promo-set pl-0 pr-0">
                <p class="promo-text">Please enter the promo code that you've received</p>
                <div class="promo-main">
                    <input name="coupon_code" class="em-apply-field" value="" type="text">
                    <input name="coupon_but" class="em-apply-but" value="Check Code" type="button">
                </div>
            </div>



        </div>


        <div class="col-sm-12 book-details-main">
            <div class="row ml-0 mr-0">
                <div class="col-7 book-det-left pl-0 pr-0">
                    <p>Price</p>
                </div>
                <div class="col-5 book-det-right pl-0 pr-0">
                    <p>AED <calc-amount class="service_amount">00.00</calc-amount></p>
                </div>
            </div>
        </div>


        <div class="col-sm-12 book-details-main">
            <div class="row ml-0 mr-0">
                <div class="col-7 book-det-left pl-0 pr-0">
                    <p>VAT 5%</p>
                </div>
                <div class="col-5 book-det-right pl-0 pr-0">
                    <p>AED <calc-amount class="vat_amount">00.00</calc-amount></p>
                </div>
            </div>
        </div>



        <div class="col-sm-12 book-details-main">
            <div class="row total-price ml-0 mr-0">
                <div class="col-7 book-det-left pl-0 pr-0">
                    <p>Total Payable</p>
                </div>
                <div class="col-5 book-det-right pl-0 pr-0">
                    <p>AED <calc-amount class="total_payable">00.00</calc-amount></p>
                </div>
            </div>
        </div>

    </div>





</div>
