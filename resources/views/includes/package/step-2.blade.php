<div class="col-sm-12 top-steps-section step-2" style="display: none">
    <div class="d-flex page-title-section">
        <div class="booking-page-title flex-grow-1">
            <h3>Date & Time</h3>
        </div>
        <div class="step-back-icon"><a href="javascript:void(0);" data-action="prev-step" data-step="2" class="back-arrow" title="Click to Back">Step
                2</a></div>
        <div class="booking-steps"> of 3</div>
    </div>
</div>
<div class="col-lg-8 col-md-12 booking-form-left step-2" style="display: none">
    <div class="col-sm-12 calender-wrapper">
        <div class="col-sm-12 p-0">
            <h4>When would you like your service? </h4>
        </div>
        <div class="col-sm-12 calendar-main p-0">
            <div id="calendar" class="owl-carousel owl-theme p-0">
            </div>
        </div>
    </div>
    <div class="col-sm-12 what-time-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>What time would you like us to start?<!--<label class="what-time-btn">See All</label>--></h4>
        </div>
        <div class="col-sm-12 booking-form-list p-0">
            <ul id="times-holder">
            </ul>
        </div>
    </div>
    <div class="col-sm-12 cleaning-materials-wrapper">
        <div class="col-sm-12 p-0 pb-2">
            <h4>Any instructions or special requirements?</h4>
        </div>
        <div class="col-sm-12 booking-form-field p-0">
            <textarea name="instructions" rows="" class="text-field-big" spellcheck="false"
                placeholder="Write here..."></textarea>
        </div>
    </div>
</div>