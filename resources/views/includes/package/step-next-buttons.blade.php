<div class="col-sm-12 booking-main-btn-section step-1">
    <div class="row m-0">
        <div class="col-sm-6 col-6 mob-total-left">
            <div class="row total-price m-0">
                <div class="col-12 book-det-left p-0">
                    <p class="p-0">Total</p>
                </div>
                <div class="col-12 book-det-right p-0">
                    <p class="p-0"><span>AED</span> <calc-amount class="total_payable">00.00</calc-amount></p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn">
            <button class="text-field-btn" type="button" data-action="next-step" data-step="1">Next</button>
        </div>
    </div>
</div>
<div class="col-sm-12 booking-main-btn-section step-2" style="display: none">
    <div class="row m-0">
        <div class="col-sm-6 col-6 mob-total-left">
            <div class="row total-price m-0">
                <div class="col-12 book-det-left p-0">
                    <p class="p-0">Total</p>
                </div>
                <div class="col-12 book-det-right p-0">
                    <p class="p-0"><span>AED</span> <calc-amount class="total_payable">00.00</calc-amount></p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn">
            <button class="text-field-btn" type="button" data-action="next-step" data-step="2">Next</button>
        </div>
    </div>
</div>
<div class="col-sm-12 booking-main-btn-section step-3" style="display: none">
    <div class="row m-0">
        <div class="col-sm-6 col-6 mob-total-left">
            <div class="row total-price m-0">
                <div class="col-12 book-det-left p-0">
                    <p class="p-0">Total</p>
                </div>
                <div class="col-12 book-det-right p-0">
                    <p class="p-0"><span>AED</span> <calc-amount class="total_payable">00.00</calc-amount></p>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn" id="submit-btn-pay-mode-1">
            <button type="submit" class="text-field-btn">Complete</button>
        </div>
        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn" id="submit-btn-pay-mode-2">
            <button type="submit" class="text-field-btn">Complete</button>
        </div>
        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn" id="submit-btn-pay-mode-3" style="display: none">
            <button type="submit" id="applePaybtn"></button>
        </div>
        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn" id="submit-btn-pay-mode-4" style="display: none">

        </div>
        <div class="col-lg-3 col-sm-6 col-6 booking-main-btn" id="submit-btn-pay-mode-5">
            <button type="submit" class="text-field-btn" disabled>Tamara Go</button>
        </div>
    </div>
</div>