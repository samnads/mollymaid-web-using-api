<div class="col-lg-4 col-md-5 booking-summary-section" id="booking-summary">
    <div class="col-sm-12 book-details-main mob-booking-title">
        <h3>Booking Summary</h3>
    </div>


    <div class="col-lg-11 col-md-12 booking-summary-main clearfix scroll">
        <div class="row m-0">
            <div class="col-sm-12 book-details-main mob-summary-title pb-2">
                <h3>Booking Summary</h3>
            </div>
            <div class="col-sm-12 book-details-main">
                @foreach ($api_package_data['packages'] as $group => $packages)
                    @foreach ($packages['data'] as $key => $package)
                        <div class="row m-0" id="package-summary-row-{{ $package['package_id'] }}" style="display: none;">
                            <div class="col-6 book-det-left ps-0 pe-0">
                                <p>{{ $package['package_name'] }}</p>
                            </div>
                            <div class="col-6 book-det-right ps-0 pe-0">
                                <div class="addon-btn-count">
                                    <input data-package_id="{{ $package['package_id'] }}" data-action="package-minus"
                                        value="" class="addon-btn-minus" type="button">
                                    <input data-package_id="{{ $package['package_id'] }}"
                                        id="package_quanity_{{ $package['package_id'] }}" min="0"
                                        max="{{ $package['cart_limit'] }}" name="package_quanity[]" value="0"
                                        class="addon-text-field no-arrow" type="number" readonly>
                                    <input data-package_id="{{ $package['package_id'] }}" data-action="package-plus"
                                        value="" class="addon-btn-plus" type="button">
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Date & Time</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p class="date">-</p>
                        <p class="time"></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row m-0">
                    <div class="col-7 book-det-left ps-0 pe-0">
                        <p>No. of Professionals</p>
                    </div>
                    <div class="col-5 book-det-right ps-0 pe-0">
                        <p class="professionals_count">1</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main instructions" style="display: none">
                <div class="row m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Instructions</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p class="instructions">-</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row book-details-main-set m-0 pt-5">
            <div class="col-sm-12 book-details-main pb-2">
                <h3>Payment Summary</h3>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Service Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="service_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main discount_total" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Discount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="discount_total">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Taxable Amount</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="taxable_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Total (inc VAT 5%)</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="taxed_amount">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main payment_type_charge" style="display: none">
                <div class="row booking-amount m-0">
                    <div class="col-6 book-det-left ps-0 pe-0">
                        <p>Convenience Fee</p>
                    </div>
                    <div class="col-6 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="payment_type_charge">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 book-details-main">
                <div class="row total-price m-0">
                    <div class="col-5 book-det-left ps-0 pe-0">
                        <p>Total</p>
                    </div>
                    <div class="col-7 book-det-right ps-0 pe-0">
                        <p><span>AED</span> <calc-amount class="total_payable">00.00</calc-amount></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
