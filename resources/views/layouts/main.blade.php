<!doctype html>
<html lang="en">

<head>
    @include('includes.google_tag')
    <title>MollyMaid</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="{{ asset('images/favicon.png') }}" />
    <!-- Bootstrap CSS -->
    @include('includes.header_assets')
    @stack('styles')
</head>

<body class="bg-color {{ @$body_css_class }}">
    @if (strpos($_SERVER['REQUEST_URI'], '-demo/') !== false || strpos($_SERVER['SERVER_NAME'], '127.0.0.1') !== false)
        <div class="demo-ribbon">
            <span>DEMO</span>
        </div>
    @endif
    @include('includes.header')
    <!--<div class="wrapper-main">
        @include('includes.header')
        @yield('content')
        @include('includes.footer')
        @include('popups.login-popup')
        @include('popups.otp-popup')
        @include('includes.footer_assets')
        @stack('scripts')
    </div>-->
</body>

</html>
