<div class="popup-main address-list-popup" id="change-pay-mode-popup">
    <form id="change-pay-mode-popup-form">
        <input name="booking_id" type="hidden" />
        <input name="PaymentMethod" type="hidden" />
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Change Payment Mode<span class="booking_id"></span></h4>
                </div>
                <div class="row m-0 mt-3">
                    <div class="col-sm-12 payment-method-wrapper p-0">
                        <div class="col-sm-12 booking-form-list payment-method p-0">
                            <ul id="payment-method-holder">
                                @foreach ($api_data['payment_types'] as $key => $payment_type)
                                    @if ($payment_type['show_in_web'] == 1)
                                        @if (!in_array($payment_type['id'], debugPaymentModes()))
                                            <li class="{{ $payment_type['code'] }}-opt">
                                                <input id="payment-method-{{ $payment_type['id'] }}"
                                                    value="{{ $payment_type['id'] }}" name="payment_method"
                                                    class="" type="radio"
                                                    {{ $payment_type['default'] == 1 ? 'checked' : '' }}>
                                                <label for="payment-method-{{ $payment_type['id'] }}">
                                                    <!-- <p>Payment by</p>{{ $payment_type['name'] }} -->
                                                    <img
                                                        src="{{ asset('images/payment-' . $payment_type['code'] . '.jpg') }}" />
                                                </label>
                                            </li>
                                        @else
                                            @if (in_array(session('customer_id'), debugPaymentModeForCustomers()))
                                                <li class="{{ $payment_type['code'] }}-opt">
                                                    <input id="payment-method-{{ $payment_type['id'] }}"
                                                        value="{{ $payment_type['id'] }}" name="payment_method"
                                                        class="" type="radio"
                                                        {{ $payment_type['default'] == 1 ? 'checked' : '' }}>
                                                    <label for="payment-method-{{ $payment_type['id'] }}">
                                                        <!-- <p>Payment by</p>{{ $payment_type['name'] }} -->
                                                        <img
                                                            src="{{ asset('images/payment-' . $payment_type['code'] . '.jpg') }}" />
                                                    </label>
                                                </li>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-sm-12 card-method-main payment-type-2 card-details" style="display: none">
                            <div class="row card-main-box m-0">
                                <div class="col-sm-12 card-method-field">
                                    <p>Card Number</p>
                                    <input name="card_number" class="text-field" type="number" value="">
                                </div>

                                <div class="col-sm-8 card-method-field">
                                    <p>Exp. Date (MM/YY)</p>
                                    <!--<input name="" class="text-field" type="number">-->
                                    <input id="datepicker" class="text-field w90 calendar" name="exp_date"
                                        type="text" autocomplete="off" data-provide="datepicker" value="">
                                </div>

                                <div class="col-sm-4 card-method-field">
                                    <p>CVV Number</p>
                                    <input name="cvv" class="text-field" type="number" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-sm-12 p-0 m-auto" id="addresses-list">
                        <div class="col-sm-12 booking-main-btn-section">
                            <div class="row m-0">
                                <div class="col-lg-5 col-md-4 col-sm-6 p-0">
                                    <button class="text-field-btn" type="submit">Complete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div><!-- Address Popup-->
