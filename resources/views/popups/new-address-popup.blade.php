<div class="popup-main add-address-popup" id="new-address-popup">
    <form id="new-address-popup-form">
        <input name="address_id" type="hidden">
        <div class="row min-vh-100 m-0">
            <div class=" mx-auto my-auto shadow popup-main-cont">
                <div class="popup-close" data-action="close"><img src="{{ asset('images/el-close-white.png') }}"
                        alt=""></div>
                <div class="col-sm-12 popup-head-text">
                    <h4>Add Address</h4>
                </div>
                <div class="row m-0">


                    <div class="col-md-6 position-relative p-0 mb-3">
                        <div class="map-search">
                            <input name="selected_address" placeholder="Seach..." class="text-field us3-address"
                                type="search">
                        </div>
                        <input name="latitude" id="latitude" type="hidden">
                        <input name="longitude" id="longitude" type="hidden">
                        <input type="hidden" class="us3-radius" value="0">
                        <div class="us3" style="height: 300px; width:100%"></div>
                    </div>


                    <!--<div clasrs="col-md-6 position-relative ps-0">
                        <div class="map-search">
                            <input name="selected_address" placeholder="Seach..." class="text-field us3-address"
                                type="search">
                        </div>

                        <input name="latitude" id="latitude" type="hidden">
                        <input name="longitude" id="longitude" type="hidden">
                        <div class="col-sm-12 address-google-map us3 p-0">
                        </div>
                    </div>-->



                    <div class="col-md-6 address-details">
                        <div class="row m-0">
                            <div class="col-sm-12 address-options p-0 pb-3">
                                <ul>
                                    @foreach ((array) @$api_data['address_types'] as $key => $address_type)
                                        <li class="home">
                                            <input id="address_type_{{ $key }}"
                                                value="{{ $address_type['code'] }}" name="address_type" class=""
                                                type="radio" {{ $address_type['code'] == 'HO' ? 'checked' : '' }}>
                                            <label
                                                for="address_type_{{ $key }}">{{ $address_type['name'] }}</label>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-sm-12 text-field-main pb-2">
                                <label for="street">Street</label>
                                <input name="street" class="text-field" type="text">
                            </div>
                            <div class="col-sm-12 p-0 pb-2">
                                <div class="row text-field-main pb-0">
                                    <div class="col-sm-6">
                                        <label>Building</label>
                                        <input name="building" class="text-field" type="text">
                                    </div>

                                    <div class="col-sm-6 m-flat-no">
                                        <label>Apartment / Villa / Office No.</label>
                                        <input name="flat_no" class="text-field" type="text">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 text-field-main pb-3">
                                <label>Area</label>
                                <select class="selectpicker form-control text-field" name="area_id"
                                    data-container="body" data-live-search="true" data-hide-disabled="true">
                                    <option value="">-- Select Area --</option>
                                    @foreach ((array) @$api_area_list['data'] as $area)
                                        <option value="{{ $area['id'] }}">{{ $area['name'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-sm-12 text-field-main pb-0">
                                <button class="text-field-btn" type="submit">Save Address</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
