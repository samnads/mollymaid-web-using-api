<div class="col-sm-12 popup-main otp-popup" id="otp-popup">
    <form id="login-otp-popup-form">
        <input name="country_code" type="hidden" value="971">
        <input name="mobilenumber" type="hidden">
        <input name="id" type="hidden">
        <div class="row min-vh-100 d-flex flex-column justify-content-center">
            <div class="col-md-2 col-sm-3 popup-content em-booking-det-cont mx-auto otp-set-main shadow">
                <h5 class="">OTP Verification <span class="em-otp-close-btn" data-action="close"><img src="images/el-close-black.png"
                            title="" style=""></span></h5>
                <p>Enter the OTP you received to<br>
                    <span class="customer-full-mobile"></span>
                </p>

                <div class="col-sm-12 em-field-main otp-set pl-0 pr-0">
                    <div class="row m-0">
                        <div class="col-12 em-text-field-main pl-0 pr-0">
                            <input name="otp" class="text-field" type="number" maxlength="4">
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 next-but pl-0 pr-0  pt-3 mb-0">
                    <button class="text-field-button show6-step" type="submit">Continue</button>
                </div>
            </div>
        </div>
    </form>
</div>
