@extends('layouts.main')
@section('title', $api_service_type_data['data']['service_type_name'])
@section('content')
    <section>
        <div class="container">
            <form id="booking-form" name="booking-form" autocomplete="off" accept-charset="utf-8">
                <div class="row inner-wrapper enquiry-form-main v-center m-0">
                    @include('includes.enquiry.enquiry')
                </div>
            </form>
        </div>
    </section>
@endsection
@push('styles')
@endpush
@push('scripts')
    <script type="text/javascript">
    var _service_type_data = @json($api_service_type_data['data']);
    var _data = @json($api_data);
    </script>
    <script type="text/javascript" src="{{ asset('js/model.js?v=') . Config::get('version.js') }}"></script>
@endpush