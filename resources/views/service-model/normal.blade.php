@extends('layouts.main', ['body_css_class' => ''])
@section('title', 'House Cleaning')
@section('content')
    <section class="em-booking-content-section">
        <form id="booking-form">
            <input name="service_type_id" value="1" type="hidden" />
            <input name="date" type="hidden" value="{{date('d/m/Y')}}" />
            <input name="address_id" type="hidden"/>
            <div class="container em-booking-content-box">
                <div class="row em-booking-content-main ml-0 mr-0">
                    <div class="col-12 em-booking-content pl-0 pr-0">
                        @if(!isWebView())
                        <div class="col-md-12 col-sm-12 em-step-heading-main no-left-right-padding">
                            <div class="col-md-12 col-sm-12 em-step-head em-head1 no-left-right-padding">
                                <h2 id="step-title">Book Your Service</h2>
                                <ul id="step-marks">
                                    <li class="mark-1 active"><span></span></li>
                                    <li class="mark-2"><span></span></li>
                                    <li class="mark-3"><span></span></li>
                                    <li class="mark-4"><span></span></li>
                                    <li class="mark-5"><span></span></li>
                                </ul>
                            </div>
                        </div>
                        @endif
                        <div class="col-12 em-booking-content-set pl-0 pr-0">
                            <div class="row em-booking-content-set-main ml-0 mr-0">
                                <div class="col-lg-8 col-md-12 col-sm-12 em-booking-content-left pl-0">
                                    @include('includes.normal.step-1')
                                    @include('includes.normal.step-2')
                                    @include('includes.normal.step-3')
                                    @include('includes.normal.step-4')
                                    @include('includes.normal.step-5')
                                </div>
                                <div class="col-lg-4 col-md-12 col-sm-12 em-booking-content-right pl-0 pr-0">
                                    @include('includes.normal.summary')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <div class="col-sm-12 em-bottom-space">&nbsp;</div><!--page bottom white space-->
    </section>
@endsection
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pignose.calendar.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.toast.css?v=1.0') }}">
@endpush
@push('scripts')
    <script type="text/javascript" src="{{ asset('js/pignose.calendar.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.toast.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/model.js?v=' . Config::get('version.js')) }}"></script>
    <script type="text/javascript" src="{{ asset('js/model.normal.js?v=' . Config::get('version.js')) }}"></script>
@endpush
