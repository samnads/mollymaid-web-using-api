<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\InvoiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['validateSessionTokenIfExist']], function () {
    Route::get('/', function () {
        return App::call('App\Http\Controllers\ServiceTypeModelController@normal_service', ['data' => []]);
    })->name('home');
    Route::get('/house-cleaning-dubai', function () {
        return App::call('App\Http\Controllers\ServiceTypeModelController@normal_service', ['data' => []]);
    });
});
Route::group(['middleware' => ['validateSessionToken']], function () {
    Route::get('/profile', [ProfileController::class, 'profile'])->name('profile');
    Route::get('/profile/personal-details', [ProfileController::class, 'personal_details']);
    Route::post('/profile/personal-details', [ProfileController::class, 'personal_details']);
    Route::get('/profile/manage-address', [ProfileController::class, 'manage_address']);
    Route::get('/bookings/{filter}', [ProfileController::class, 'bookings']);
    Route::get('/booking/success/{reference_id}', [ProfileController::class, 'booking_success'])->name('booking-success');
    Route::get('/booking/failed/{reference_id}', [ProfileController::class, 'booking_failed'])->name('booking-failed');
    // Route::post('/profile-popup', [ProfileController::class,'profile_edit_popup']);

});
Route::get('invoice-payment', [InvoiceController::class, 'invoice_payment_entry']);
Route::post('save-invoice-pay', [InvoiceController::class, 'invoice_payment_save']);
Route::any('/api/customer/{endpoint}', [ApiController::class, 'customer_api_call']);
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    if($exitCode == 0){
        return "Cache cleared successfully !";
    }
});
